addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.9")

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.24")

addSbtPlugin("org.clapper" % "sbt-editsource" % "1.0.0")

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.6.0")

addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.4.2")
