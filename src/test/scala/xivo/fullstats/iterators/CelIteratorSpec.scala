package xivo.fullstats.iterators

import org.scalatest.{FlatSpec, Matchers}
import xivo.fullstats.testutils.DBUtil

class CelIteratorSpec extends FlatSpec with Matchers {
  "CelIterator.createStatement" should "query the database with the startId, the maxId and the addtionnalCids" in {
    val connection = DBUtil.getConnection
    val it = new CelIterator(45, 128, List("123.456", "789.123"), connection)
    val st = it.createStatement()

    st.toString shouldEqual "((select * from cel where id >= 45 AND id <= 128 order by id asc) UNION select * from cel where  linkedid in ( '123.456','789.123' ))" +
      " order by id asc"
  }
}
