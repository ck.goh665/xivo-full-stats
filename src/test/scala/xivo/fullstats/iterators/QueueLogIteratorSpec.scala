package xivo.fullstats.iterators

import xivo.fullstats.testutils.{BaseTest, DBUtil}

class QueueLogIteratorSpec extends BaseTest {
  "QueueLogIterator.createStatement" should {
    "query the database with the startId, the maxId and the addtionnalCids" in {
      val connection = DBUtil.getConnection
      val it = new QueueLogIterator(45, 128, List("123.456", "789.123"), connection)
      val st = it.createStatement()

      st.toString shouldEqual "SELECT id, to_timestamp(time, 'YYYY-MM-DD HH24:MI:SS.US') AS time, callid, queuename, agent, event, data1, data2, data3, data4, data5" +
        " FROM queue_log WHERE id >= 45 AND id <= 128 OR callid IN ( '123.456','789.123' ) ORDER BY id ASC"
    }
  }
}
