package xivo.fullstats.iterators

import java.sql.Connection

import org.scalatest.Assertion
import org.scalatest.concurrent.ScalaFutures
import xivo.fullstats.model._
import xivo.fullstats.testutils.{CelTestUtils, DBTestShould, DBUtil}
import xivo.fullstats.{CallEventList, ConnectionFactory, EmptyObjectProvider}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import org.scalatest.easymock.EasyMockSugar

class CallEventIteratorSpec extends DBTestShould(List("cel", "queue_log")) with EasyMockSugar with ScalaFutures {

  class Helper {
    val cnx = new ConnectionFactory(DBUtil.HOST, DBUtil.DB_NAME, DBUtil.USER, DBUtil.PASSWORD)
    val closingHolder = new ClosingHolder

    private def withElts[T](f: T => Unit)(all: Seq[T]) = all.toList.foreach(f)

    def withQueueLogs(qls: QueueLog*) = withElts(QueueLogTestUtils.insertQueueLog)(qls)

    def withCels(cels: Cel*) = withElts(CelTestUtils.insertCel)(cels)

  }

  trait QueueLogSamples {
    val ql0 = QueueLog(40, format.parseDateTime("2013-01-01 07:59:00"), "13455.789", "queue1", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql1 = QueueLog(41, format.parseDateTime("2013-01-01 08:00:00"), "13456.789", "queue1", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql2 = QueueLog(42, format.parseDateTime("2013-01-01 08:10:00"), "13456.789", "queue1", "agent1", "CONNECT", None, None, None, None, None)
    val ql3 = QueueLog(43, format.parseDateTime("2013-01-01 08:20:00"), "13456.790", "queue2", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql4 = QueueLog(44, format.parseDateTime("2013-01-01 08:30:00"), "13456.790", "queue2", "agent1", "CONNECT", None, None, None, None, None)
    val ql5 = QueueLog(45, format.parseDateTime("2013-01-01 08:32:00"), "13455.789", "queue1", "NONE", "ABANDON", None, None, None, None, None)
  }

  trait CelSamples {
    val additionalCelCid = "1392999905.102"

    val c1 = EmptyObjectProvider.emptyCel().copy(id = 10, linkedId = "123456.788", uniqueId = "123456.788", eventType = "LINKEDID_END",
      eventTime = formatMs.parseDateTime("2013-01-01 08:10:11.312"))
    val c2 = EmptyObjectProvider.emptyCel().copy(id = 11, linkedId = additionalCelCid, uniqueId = additionalCelCid, eventType = "APP_START",
      eventTime = formatMs.parseDateTime("2013-01-01 08:13:11.312"))
    val c3 = EmptyObjectProvider.emptyCel().copy(id = 12, linkedId = "123456.789", uniqueId = "123456.789", eventType = "CHAN_START",
      eventTime = formatMs.parseDateTime("2013-01-01 08:21:11.312"))
    val c4 = EmptyObjectProvider.emptyCel().copy(id = 13, linkedId = "123456.789", uniqueId = "123456.789", eventType = "XIVO_INCALL",
      eventTime = formatMs.parseDateTime("2013-01-01 08:31:11.312"))
    val c5 = EmptyObjectProvider.emptyCel().copy(id = 14, linkedId = "123456.789", uniqueId = "123456.789", eventType = "CHAN_END",
      eventTime = formatMs.parseDateTime("2013-01-01 08:33:11.312"))

  }

  "A CallEventIterator" should {

    "not ignore queue log milliseconds" in new Helper with CelSamples {
      val ql0 = QueueLog(40, formatMs.parseDateTime("2013-01-01 07:59:00.907"), "13455.789", "queue1", "NONE", "ENTERQUEUE", None, None, None, None, None)
      withQueueLogs(ql0)
      withCels(c1)

      val it = new CallEventIterator(0, 0, List(), List(), closingHolder)(cnx)

      it.next().time shouldEqual formatMs.parseDateTime("2013-01-01 07:59:00.907")
      closingHolder.close()
    }


    "retrieve and iterate over call events in the right order, until told to stop" in new Helper with QueueLogSamples with CelSamples {
      val startQlId = 42
      val additionalQlCid = "13455.789"
      withQueueLogs(ql0, ql3, ql2, ql4, ql1, ql5)

      val startCelId = 12
      withCels(c1, c2, c3, c4, c5)

      val it = new CallEventIterator(startCelId, startQlId, List(additionalCelCid), List(additionalQlCid), closingHolder)(cnx)

      it.next() shouldEqual ql0
      it.next() shouldEqual ql2
      it.next() shouldEqual c2
      it.next() shouldEqual ql3
      it.next() shouldEqual c3
      it.next() shouldEqual ql4
      it.next() shouldEqual c4
      it.next() shouldEqual ql5
      it.next() shouldEqual c5
      closingHolder.close()
      it.hasNext shouldBe false
    }
  }

  "get some data when they become available" in new Helper with QueueLogSamples with CelSamples {
    withQueueLogs(ql0)

    withCels(c1)

    val evtIterator = new CallEventIterator(0, 0, List(), List(), closingHolder)(cnx)

    val it = evtIterator
    it.hasNext shouldBe true
    it.next() shouldEqual ql0
    it.hasNext shouldBe true
    it.next() shouldEqual c1

    QueueLogTestUtils.insertQueueLog(ql1)
    CelTestUtils.insertCel(c2)
    it.searchNewData()

    it.hasNext shouldBe true
    it.next() shouldEqual ql1
    it.hasNext shouldBe true
    it.next() shouldEqual c2
    it.hasNext shouldBe false

    closingHolder.close()

    closingHolder.isClosed shouldBe true
  }

  "infer the max queue log id from the max cel id" in new Helper {
    val celMock = mock[CelInterface]
    val qlMock = mock[QueueLogInterface]
    val localConn = mock[Connection]

    val startCelId = 12
    val startQlId = 5

    val it = new CallEventIterator(startCelId, startQlId, List(), List(), closingHolder)(cnx)
    it.connection = localConn
    it.cel = celMock
    it.queueLog = qlMock
    celMock.maximumId(localConn).andReturn(Some(51000))
    celMock.minimumId(localConn).andReturn(Some(100))
    celMock.eventTimeById(50100)(localConn).andReturn(Some(format.parseDateTime("2015-01-01 08:00:00")))
    qlMock.idClosestToTime(format.parseDateTime("2015-01-01 08:00:00"))(localConn).andReturn(Some(40000))

    whenExecuting(localConn, celMock, qlMock) {
      it.setMaxIds()
      it.maxIds shouldEqual MaxIds(maxCelId = 50100, maxQueueLogId = 40000)
    }
  }
}
