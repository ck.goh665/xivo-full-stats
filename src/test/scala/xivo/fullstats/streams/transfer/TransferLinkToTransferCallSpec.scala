package xivo.fullstats.streams.transfer

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.{CallChannel, TransferCall, TransferLinks}
import xivo.fullstats.testutils.{CallTestUtils, CelTestUtils, DBTest}

class TransferLinkToTransferCallSpec  extends DBTest(List("xc_call_channel", "cel")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val transferRepository = new TransferRepository

    val transferTimeCel = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val linkedId = "987654.321"

    val transferCallCreator = new TransferLinkToTransferCall
    val flowUnderTest = transferCallCreator.flow

    val testSink = TestSink.probe[TransferCall]

    def streamUnderTest(transferLinks: List[TransferLinks]) = Source(List(transferLinks)).via(flowUnderTest)

    val transferLink = TransferLinks(linkedId = linkedId, collectedUniqueIds = List(), transferDecisionId = None,
      onHold = None, transferTime = None, doSaveToDatabase = false, stopCollecting = false)

    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = transferTimeCel, endTime = None,
      uniqueId = uniqueId, termination = None)

    val callChannel1 = callChannel.copy(uniqueId = "1111.111")
    val callChannel2 = callChannel.copy(uniqueId = "1111.222")
    val callChannel3 = callChannel.copy(uniqueId = "1111.333")
    val callChannel4 = callChannel.copy(uniqueId = "1111.444")

    val insertedCallChannel1 = CallChannel.insert(callChannel1)
    val insertedCallChannel2 = CallChannel.insert(callChannel2)
    val insertedCallChannel3 = CallChannel.insert(callChannel3)
    val insertedCallChannel4 = CallChannel.insert(callChannel4)
  }

  "TransferCallCreator Flow" should "create attended transfer call" in new Helper {
    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(uniqueId = "1111.444", linkedId = "1111.333"))

    val validTransferLink = transferLink.copy(linkedId = "1111.111",
      collectedUniqueIds = List("1111.111", "1111.222", "1111.333", "1111.444"),
      transferDecisionId = Some("1111.111"),
      onHold = Some("1111.222"),
      transferTime = Some(transferTimeCel)
    )

    transferRepository.addToMap("1111.111", List(validTransferLink))

    val expected = TransferCall(None, insertedCallChannel1.id.get, insertedCallChannel4.id.get,
      insertedCallChannel2.id.get, Some(insertedCallChannel3.id.get), transferTimeCel)

    streamUnderTest(List(validTransferLink)).runWith(testSink).request(1).expectNext(expected).expectComplete()
    transferRepository.repository.isEmpty shouldBe true
  }

  it should "create attended transfer call with empty on talk" in new Helper {
    val validTransferLink = transferLink.copy(linkedId = "1111.111",
      collectedUniqueIds = List("1111.111", "1111.222", "1111.333", "1111.444"),
      transferDecisionId = Some("1111.111"),
      onHold = Some("1111.222"),
      transferTime = Some(transferTimeCel)
    )

    transferRepository.addToMap("1111.111", List(validTransferLink))

    val expected = TransferCall(None, insertedCallChannel1.id.get, insertedCallChannel4.id.get,
      insertedCallChannel2.id.get, None, transferTimeCel)

    streamUnderTest(List(validTransferLink)).runWith(testSink).request(1).expectNext(expected).expectComplete()
    transferRepository.repository.isEmpty shouldBe true
  }

  it should "create blind transfer call with empty on talk" in new Helper {
    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(uniqueId = "1111.444", linkedId = "1111.111"))

    val validTransferLink = transferLink.copy(linkedId = "1111.111",
      collectedUniqueIds = List("1111.111", "1111.222", "1111.333", "1111.444"),
      transferDecisionId = Some("1111.111"),
      onHold = Some("1111.222"),
      transferTime = Some(transferTimeCel)
    )

    transferRepository.addToMap("1111.111", List(validTransferLink))

    val expected = TransferCall(None, insertedCallChannel1.id.get, insertedCallChannel4.id.get,
      insertedCallChannel2.id.get, None, transferTimeCel)

    streamUnderTest(List(validTransferLink)).runWith(testSink).request(1).expectNext(expected).expectComplete()
    transferRepository.repository.isEmpty shouldBe true
  }

  it should "ignore not defined transfer link" in new Helper {
    streamUnderTest(List(transferLink)).runWith(testSink).request(1).expectComplete()
  }
}