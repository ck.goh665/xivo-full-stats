package xivo.fullstats.streams.transfer.cel

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.{AttendedTransferCel, TransferLinks}
import xivo.fullstats.streams.transfer.TransferRepository
import xivo.fullstats.testutils.DBTest

class AttendedTransferSpec extends DBTest(List("xc_call_channel")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val transferRepository = new TransferRepository

    val transferTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val linkedId = "987654.321"
    val channelName = "SIP/larerx9a-00000008"

    val attendedTransfer = new AttendedTransfer
    val flowUnderTest = attendedTransfer.flow

    val testSink = TestSink.probe[List[TransferLinks]]

    def streamUnderTest(cel: AttendedTransferCel) = Source(List(cel)).via(flowUnderTest)

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = transferTime, linkedId = linkedId, chanName = channelName,
      uniqueId = uniqueId, eventType = "ATTENDEDTRANSFER", context = "default", appName = "AppDial")

    val transferLink = TransferLinks(linkedId = linkedId, collectedUniqueIds = List(), transferDecisionId = None,
      onHold = None, transferTime = None, doSaveToDatabase = false, stopCollecting = false)
  }

  "AttendedTransfer Flow" should "create first attended transfer" in new Helper {
    val attendedTransferCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId)

    transferRepository.addToMap(linkedId, List(transferLink))

    val expected = List(transferLink.copy(collectedUniqueIds = List(), transferDecisionId = Some(linkedId),
      onHold = Some(uniqueId), transferTime = Some(transferTime)))

    streamUnderTest(AttendedTransferCel(attendedTransferCel)).runWith(testSink).request(2).expectNext(expected).expectComplete()
  }

  it should "create second attended transfers" in new Helper {
    val attendedTransferCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId)

    val firstTransferLink = transferLink.copy(collectedUniqueIds = List(), transferDecisionId = Some(linkedId),
      onHold = Some(uniqueId), transferTime = Some(transferTime))
    val secondTransferLink = transferLink.copy(collectedUniqueIds = List(), transferDecisionId = Some(linkedId),
      onHold = Some(uniqueId), transferTime = Some(transferTime))

    transferRepository.addToMap(linkedId, List(firstTransferLink))

    val expected = List(
      firstTransferLink.copy(stopCollecting = true),
      secondTransferLink)

    streamUnderTest(AttendedTransferCel(attendedTransferCel)).runWith(testSink).request(2).expectNext(expected).expectComplete()
  }

  it should "create three attended transfers" in new Helper {
    val attendedTransferCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId)

    val firstTransferLink = transferLink.copy(collectedUniqueIds = List(), transferDecisionId = Some(linkedId),
      onHold = Some(uniqueId), transferTime = Some(transferTime), stopCollecting = true)
    val secondTransferLink = transferLink.copy(collectedUniqueIds = List(), transferDecisionId = Some(linkedId),
      onHold = Some(uniqueId), transferTime = Some(transferTime))
    val thirdTransferLink = transferLink.copy(collectedUniqueIds = List(), transferDecisionId = Some(linkedId),
      onHold = Some(uniqueId), transferTime = Some(transferTime))

    transferRepository.addToMap(linkedId, List(firstTransferLink, secondTransferLink))

    val expected = List(
      firstTransferLink,
      secondTransferLink.copy(stopCollecting = true),
      thirdTransferLink)

    streamUnderTest(AttendedTransferCel(attendedTransferCel)).runWith(testSink).request(3).expectNext(expected).expectComplete()
  }

  it should "return empty list if no transfer link is found" in new Helper {
    val bridgeEnterCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId)

    val expected = List()

    streamUnderTest(AttendedTransferCel(bridgeEnterCel)).runWith(testSink).request(2).expectNext(expected).expectComplete()
  }
}
