package xivo.fullstats.streams.transfer.cel

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.{TransferLinks, XivoAttendedTransferEndCel}
import xivo.fullstats.streams.transfer.TransferRepository
import xivo.fullstats.testutils.DBTest

class XivoAttendedTransferEndSpec  extends DBTest(List("xc_call_channel")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val transferRepository = new TransferRepository

    val transferTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val linkedId = "987654.321"
    val channelName = "SIP/larerx9a-00000008"

    val xivoAttendedTransferEnd = new XivoAttendedTransferEnd
    val flowUnderTest = xivoAttendedTransferEnd.flow

    val testSink = TestSink.probe[List[TransferLinks]]

    def streamUnderTest(cel: XivoAttendedTransferEndCel) = Source(List(cel)).via(flowUnderTest)

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = transferTime, linkedId = linkedId, chanName = channelName,
      uniqueId = uniqueId, eventType = "XIVO_ATTENDEDTRANSFER_END", context = "default", appName = "AppDial")

    val transferLink = TransferLinks(linkedId = linkedId, collectedUniqueIds = List(), transferDecisionId = None,
      onHold = None, transferTime = None, doSaveToDatabase = false, stopCollecting = false)
  }

  "XivoAttendedTransferEnd Flow" should "update transfer link" in new Helper {
    val xivoAttendedTransferEndCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId)

    transferRepository.addToMap(linkedId, List(transferLink))

    val expected = List(transferLink.copy(collectedUniqueIds = List(), transferDecisionId = Some(linkedId),
      onHold = None, transferTime = Some(transferTime)))

    streamUnderTest(XivoAttendedTransferEndCel(xivoAttendedTransferEndCel)).runWith(testSink).request(2).expectNext(expected).expectComplete()
  }

  it should "return empty list if no transfer link is found" in new Helper {
    val xivoAttendedTransferEndCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId)

    val expected = List()

    streamUnderTest(XivoAttendedTransferEndCel(xivoAttendedTransferEndCel)).runWith(testSink).request(2).expectNext(expected).expectComplete()
  }
}
