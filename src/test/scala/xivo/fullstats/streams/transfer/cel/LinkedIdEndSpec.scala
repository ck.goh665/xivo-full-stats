package xivo.fullstats.streams.transfer.cel

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.{LinkedIdEndCel, TransferLinks}
import xivo.fullstats.streams.transfer.TransferRepository
import xivo.fullstats.testutils.DBTest

class LinkedIdEndSpec  extends DBTest(List("xc_call_channel")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val transferRepository = new TransferRepository

    val endTimeCaller = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val linkedId = "987654.321"
    val channelName = "SIP/larerx9a-00000008"

    val linkedIdEnd = new LinkedIdEnd
    val flowUnderTest = linkedIdEnd.flow

    val testSink = TestSink.probe[List[TransferLinks]]

    def streamUnderTest(cel: LinkedIdEndCel) = Source(List(cel)).via(flowUnderTest)

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = endTimeCaller, linkedId = linkedId, chanName = channelName,
      uniqueId = uniqueId, eventType = "LINKEDID_END", context = "default", appName = "AppDial")

    val transferLink = TransferLinks(linkedId = linkedId, collectedUniqueIds = List(), transferDecisionId = None,
      onHold = None, transferTime = None, doSaveToDatabase = false, stopCollecting = false)
  }

  "LinkedIdEnd Flow" should "delete transfer link from map" in new Helper {
    val linkedIdEndCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId)

    transferRepository.addToMap(linkedId, List(transferLink))

    streamUnderTest(LinkedIdEndCel(linkedIdEndCel)).runWith(testSink).request(2).expectNext(List(transferLink)).expectComplete()

    transferRepository.repository.isEmpty shouldBe true
  }
}
