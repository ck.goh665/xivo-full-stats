package xivo.fullstats.streams.callChannel.cel

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import akka.testkit.TestProbe
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.CallChannelRepository
import xivo.fullstats.testutils.{CallTestUtils, DBTest}

import scala.collection.mutable
import scala.concurrent.duration._

class XivoIncallSpec extends DBTest(List("cel"))  {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val celRepository = new CallChannelRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val uniqueId = "123456.789"
    val callChannel = CallTestUtils.createCallChannel(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId, termination = None)

    val probe = TestProbe()

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = startTime, linkedId = uniqueId, uniqueId = uniqueId, eventType = "XIVO_INCALL")
  }

  "XivoIncall Flow" should "change the call direction to incoming" in new Helper {
    val xivoIncall = new XivoIncall
    val flowUnderTest = xivoIncall.flow

    celRepository.addCallToMap(uniqueId, callChannel)

    val expectedCallChannel = callChannel.copy(emitted = false, scope = CallScope.External)
    val expectedMap = mutable.HashMap(uniqueId -> expectedCallChannel)

    val future = Source(List(XivoIncallCel(cel))).via(flowUnderTest).to(Sink.actorRef(probe.ref, "")).run()

    probe.expectMsg(2.seconds, expectedCallChannel)
    celRepository.repository shouldEqual expectedMap
  }
}
