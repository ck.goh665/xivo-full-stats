package xivo.fullstats.streams.source

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.testkit.scaladsl.TestSink
import org.joda.time.DateTime
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.iterators.ClosingHolder
import xivo.fullstats.model.{QueueCall, QueueLog, QueueLogTestUtils, QueueTerminationType}
import xivo.fullstats.testutils.{CallTestUtils, DBTest, DBUtil}

class QueueLogSourceSpec extends DBTest(List("queue_log", "xc_queue_call")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    val startTime: DateTime = format.parseDateTime("2019-01-01 12:00:00")
    val endTime: DateTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"

    val closingHolder = new ClosingHolder

    val queueCall = CallTestUtils.createQueueCall(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId)

    val ql: QueueLog = EmptyObjectProvider.emptyQueueLog().copy(callid = uniqueId, event = "ENTERQUEUE", time = startTime, queueName = "queue1")

    val testSink = TestSink.probe[QueueLog]
  }

  "QueueLogSourceSpec" should "delete pending queue calls" in new Helper {
    val insertedQueueCalls: List[QueueCall] = {
      (for (_ <- 1 to 5) yield QueueCall.insertOrUpdate(queueCall)).toList }

    var res = QueueCall.getPendingUniqueIds(startTime)

    res shouldEqual  insertedQueueCalls.map(_.uniqueId)
    res.size shouldEqual 5

    val qlSource = new QueueLogSource(0, closingHolder, Some(startTime), DBUtil.getConnection)
    res = QueueCall.getPendingUniqueIds(startTime)

    res.isEmpty shouldBe true
    res.size shouldEqual 0
  }

  it should "set stale pending queue calls as stalled" in new Helper {
    val insertedQueueCalls: List[QueueCall] = {
      (for (i <- 1 to 5) yield QueueCall.insertOrUpdate(queueCall.copy(uniqueId = uniqueId + i))).toList }
    val insertedMostRecentQueueCall: QueueCall = QueueCall.insertOrUpdate(queueCall.copy(startTime = startTime.plusDays(5), endTime = None))

    QueueCall.setStalePendingCalls()

    QueueCall.getPendingUniqueIds(startTime).size shouldEqual 6
    QueueCall.getByUniqueId(uniqueId).get shouldEqual insertedMostRecentQueueCall
    val res = for (i <- 1 to 5) yield QueueCall.getByUniqueId(uniqueId + i).get
    res shouldEqual insertedQueueCalls.map(c => c.copy(endTime = Some(startTime), termination = Some(QueueTerminationType.STALLED)))
  }

  it should "emit queue logs" in new Helper {
    val toInsertQueueLogs: List[QueueLog] = (for (i <- 1 to 5) yield ql.copy(id = i)).toList
    toInsertQueueLogs.foreach(QueueLogTestUtils.insertQueueLog)

    val queueLogSource = new QueueLogSource(0, closingHolder, Some(startTime), DBUtil.getConnection)

    queueLogSource.queueLogSource.runWith(testSink).request(5).expectNextN(toInsertQueueLogs)
  }

  it should "emit pending queue logs" in new Helper {
    val insertedQueueCalls: List[QueueCall] = {
      (for (_ <- 1 to 5) yield QueueCall.insertOrUpdate(queueCall)).toList }

    val toInsertPendingQueueLogs: List[QueueLog] = (for (i <- 1 to 5) yield ql.copy(id = i)).toList
    toInsertPendingQueueLogs.foreach(QueueLogTestUtils.insertQueueLog)

    val queueLogSource = new QueueLogSource(999, closingHolder, Some(startTime), DBUtil.getConnection)

    queueLogSource.pendingQueueLogsSource.runWith(testSink).request(5).expectNextN(toInsertPendingQueueLogs)
  }

  it should "emit no pending queue logs" in new Helper {
    val insertedQueueCalls: List[QueueCall] = {
      (for (_ <- 1 to 5) yield QueueCall.insertOrUpdate(queueCall)).toList }

    val toInsertPendingQueueLogs: List[QueueLog] = (for (i <- 1 to 5) yield ql.copy(id = i)).toList
    toInsertPendingQueueLogs.foreach(QueueLogTestUtils.insertQueueLog)

    val queueLogSource = new QueueLogSource(999, closingHolder, Some(startTime.plusHours(1)), DBUtil.getConnection)

    queueLogSource.pendingQueueLogsSource.runWith(testSink).request(1).expectComplete()
  }

  it should "emit no stalled queue logs" in new Helper {
  val insertedQueueCalls: List[QueueCall] = {
      (for (i <- 1 to 5) yield {
        QueueCall.insertOrUpdate(queueCall.copy(uniqueId = uniqueId+i, startTime = startTime.plusHours(i)))
      }).toList }
    val insertedStalledQueueCall = QueueCall.insert(queueCall.copy(uniqueId = uniqueId+6, startTime = startTime.minusDays(2)))

    val startFrom = QueueCall.getPendingStartTime()

    val toInsertQueueLogs: List[QueueLog] = (for (i <- 1 to 6) yield ql.copy(id = i, callid = uniqueId+i)).toList
    toInsertQueueLogs.foreach(QueueLogTestUtils.insertQueueLog)

    val queueLogSource = new QueueLogSource(999, closingHolder, startFrom, DBUtil.getConnection)

    queueLogSource.pendingQueueLogsSource.runWith(testSink).request(6).expectNextN(toInsertQueueLogs.dropRight(1)).expectComplete()
  }

  it should "emit queue logs together with pending cel" in new Helper {
    val insertedQueueCalls: List[QueueCall] = {
      (for (_ <- 1 to 5) yield QueueCall.insertOrUpdate(queueCall)).toList }

    val toInsertPendingQueueLogs: List[QueueLog] = (for (i <- 1 to 5) yield ql.copy(id = i)).toList
    toInsertPendingQueueLogs.foreach(QueueLogTestUtils.insertQueueLog)

    val toInsertQueueLogs: List[QueueLog] = (for (i <- 6 to 10) yield ql.copy(id = i)).toList
    toInsertQueueLogs.foreach(QueueLogTestUtils.insertQueueLog)

    val queueLogSource = new QueueLogSource(2, closingHolder, Some(startTime), DBUtil.getConnection)

    queueLogSource.source.runWith(testSink).request(10).expectNextN(toInsertPendingQueueLogs ++ toInsertQueueLogs)
  }

  it should "complete" in new Helper {
    val queueLogSource = new QueueLogSource(0, closingHolder, Some(startTime), DBUtil.getConnection)

    closingHolder.close()

    queueLogSource.source.runWith(testSink).expectSubscriptionAndComplete()

  }
}

