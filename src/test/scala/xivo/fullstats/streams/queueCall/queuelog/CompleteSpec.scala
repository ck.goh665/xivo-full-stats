package xivo.fullstats.streams.queueCall.queuelog

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.queueCall.QueueCallRepository
import xivo.fullstats.testutils.{CallTestUtils, DBTest}

import scala.collection.mutable

class CompleteSpec extends DBTest(List("queue_log")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val queueCallRepository = new QueueCallRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val answerTime = format.parseDateTime("2019-01-01 12:03:00")
    val endTime = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"
    val queueName = "queue1"
    val agent = "Agent/2001"

    val queueCall = CallTestUtils.createQueueCall(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId, answerTime = Some(answerTime))

    val completeGeneric = new Complete
    val flowUnderTest = completeGeneric.flow

    val testSink = TestSink.probe[QueueCall]
    def streamUnderTest(ql: CompleteQl) = Source(List(ql)).via(flowUnderTest)

    val ql: QueueLog = EmptyObjectProvider.emptyQueueLog().copy(id = 1, time = endTime, callid = uniqueId,
      event = "COMPLETEAGENT", queueName = queueName, agent = agent)
  }

  "Complete Flow" should "update end time in queue call repository" in new Helper {
    val blankTerminationQl = ql.copy(event = "")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(endTime = Some(endTime), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(blankTerminationQl)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "update unanswered ring time in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "ABANDON", time = endTime)

    val unAnsweredQueueCall = queueCall.copy(answerTime = None)

    queueCallRepository.addCallToMap(uniqueId, unAnsweredQueueCall)

    val expectedQueueCall = unAnsweredQueueCall.copy(endTime = Some(endTime),
      termination = Some(QueueTerminationType.ABANDON), ringDuration = Some(300), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(qlWithTermination)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "update COMPLETEAGENT termination in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "COMPLETEAGENT")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(endTime = Some(endTime), termination = Some(QueueTerminationType.COMPLETEAGENT), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(qlWithTermination)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "update COMPLETECALLER termination in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "COMPLETECALLER")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(endTime = Some(endTime), termination = Some(QueueTerminationType.COMPLETECALLER), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(qlWithTermination)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "update COMPLETECALLER termination in queue call repository (transfer from queue to queue)" in new Helper {
    val qlWithTerminationWithWrongCallid = ql.copy(event = "COMPLETECALLER", callid = "111111.222")
    val qcTransferred = queueCall.copy(transferredCalls = Map("111111.222" -> uniqueId))

    queueCallRepository.addCallToMap(uniqueId, qcTransferred)

    val expectedQueueCall = qcTransferred.copy(endTime = Some(endTime), termination = Some(QueueTerminationType.COMPLETECALLER), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(qlWithTerminationWithWrongCallid)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "update ABANDON termination in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "ABANDON")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(endTime = Some(endTime), termination = Some(QueueTerminationType.ABANDON), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(qlWithTermination)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "update EXITWITHKEY termination in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "EXITWITHKEY")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(endTime = Some(endTime), termination = Some(QueueTerminationType.EXITWITHKEY), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(qlWithTermination)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "update EXITWITHTIMEOUT termination in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "EXITWITHTIMEOUT")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(endTime = Some(endTime), termination = Some(QueueTerminationType.EXITWITHTIMEOUT), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(qlWithTermination)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "update CLOSED termination in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "CLOSED")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(endTime = Some(endTime), termination = Some(QueueTerminationType.CLOSED), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(qlWithTermination)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "update DIVERT_CA_RATIO termination in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "DIVERT_CA_RATIO")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(endTime = Some(endTime), termination = Some(QueueTerminationType.DIVERT_CA_RATIO), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(qlWithTermination)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "update DIVERT_HOLDTIME termination in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "DIVERT_HOLDTIME")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(endTime = Some(endTime), termination = Some(QueueTerminationType.DIVERT_HOLDTIME), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(qlWithTermination)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "update FULL termination in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "FULL")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(endTime = Some(endTime), termination = Some(QueueTerminationType.FULL), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(qlWithTermination)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "update LEAVEEMPTY termination in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "LEAVEEMPTY")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(endTime = Some(endTime), termination = Some(QueueTerminationType.LEAVEEMPTY), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(qlWithTermination)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "update JOINEMPTY termination in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "JOINEMPTY")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(endTime = Some(endTime), termination = Some(QueueTerminationType.JOINEMPTY), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(qlWithTermination)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "update BLINDTRANSFER termination in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "BLINDTRANSFER")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(endTime = Some(endTime), termination = Some(QueueTerminationType.BLINDTRANSFER), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)
    streamUnderTest(CompleteQl(qlWithTermination)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "update CLOSED termination to COMPLETEAGENT in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "COMPLETEAGENT")

    val queueCallWithTermination = queueCall.copy(endTime = Some(endTime), termination = Some(QueueTerminationType.CLOSED))

    queueCallRepository.addCallToMap(uniqueId, queueCallWithTermination)

    val expectedQueueCall = queueCall.copy(endTime = Some(endTime), termination = Some(QueueTerminationType.COMPLETEAGENT), deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(qlWithTermination)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }

  it should "not update NONEXISTING termination in queue call repository" in new Helper {
    val qlWithTermination = ql.copy(event = "NONEXISTING")

    queueCallRepository.addCallToMap(uniqueId, queueCall)

    val expectedQueueCall = queueCall.copy(endTime = Some(endTime), termination = None, deleted = true)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    streamUnderTest(CompleteQl(qlWithTermination)).runWith(testSink).request(2).expectNext(expectedQueueCall).expectComplete()
    queueCallRepository.repository.isEmpty shouldEqual true
  }
}
