package xivo.fullstats.streams.queueCall.queuelog

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import akka.testkit.TestProbe
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.queueCall.QueueCallRepository
import xivo.fullstats.testutils.{CallTestUtils, DBTest}

import scala.collection.mutable
import scala.concurrent.duration._

class EnterQueueSpec extends DBTest(List("queue_log")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val queueCallRepository = new QueueCallRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val uniqueId = "123456.789"
    val channelName = "SIP/larerx9a-00000008"
    val queueName = "queue1"

    val queueCall = CallTestUtils.createQueueCall(id = None, startTime = startTime, endTime = None,
      uniqueId = uniqueId)

    val probe = TestProbe()

    val ql: QueueLog = EmptyObjectProvider.emptyQueueLog().copy(id = 1, time = startTime, callid = uniqueId,
      event = "ENTERQUEUE", queueName = queueName)
  }

  "EnterQueue Flow" should "create queue call in the repository" in new Helper {
    val enterQueue = new EnterQueue
    val flowUnderTest = enterQueue.flow

    val expectedQueueCall = queueCall.copy(queueRef = queueName, uniqueId = uniqueId)
    val expectedMap = mutable.HashMap(uniqueId -> expectedQueueCall)

    Source(List(EnterQueueQl(ql))).via(flowUnderTest).to(Sink.actorRef(probe.ref, "")).run()

    probe.expectMsg(2.seconds, expectedQueueCall)
    queueCallRepository.repository shouldEqual expectedMap
  }
}
