package xivo.fullstats.streams.conversation.cel

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Keep}
import akka.stream.testkit.scaladsl.{TestSink, TestSource}
import akka.testkit.TestProbe
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.conversation.ConversationRepository
import xivo.fullstats.testutils.DBTest

class LinkedIdEndSpec extends DBTest(List("xc_call_channel")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val conversationRepository = new ConversationRepository

    val startTime = format.parseDateTime("2019-01-01 12:00:00")
    val endTimeCaller = format.parseDateTime("2019-01-01 12:05:00")
    val endTimeCallee = format.parseDateTime("2019-01-01 12:05:10")
    val uniqueId = "123456.789"
    val linkedId = "987654.321"
    val channelName = "SIP/larerx9a-00000008"

    val linkedIdEnd = new LinkedIdEnd
    val flowUnderTest = linkedIdEnd.flow

    val probe = TestProbe()

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = endTimeCaller, linkedId = linkedId, chanName = channelName,
      uniqueId = uniqueId, eventType = "LINKEDID_END", context = "default", appName = "AppDial")

    def testStream(flowUnderTest: Flow[LinkedIdEndCel, ConversationLinks, NotUsed]) = {
      TestSource.probe[LinkedIdEndCel].via(flowUnderTest).toMat(TestSink.probe)(Keep.both).run()
    }
  }

  "LinkedIdEnd Flow" should "delete conversation links from map" in new Helper {
    val linkedIdEndCel = cel.copy(linkedId = "123456.789")

    val conversationLinks = ConversationLinks("123456.789", List("987654.321"))

    conversationRepository.addToMap("123456.789", conversationLinks)

    val (source, sink) = testStream(flowUnderTest)

    conversationRepository.repository.isEmpty shouldBe false
    sink.request(1)
    source.sendNext(LinkedIdEndCel(linkedIdEndCel))
    sink.expectNext(conversationLinks)
    source.sendComplete()
    conversationRepository.repository.isEmpty shouldBe true
  }
}
