package xivo.fullstats.streams.conversation.cel

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import akka.stream.testkit.scaladsl.TestSink
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.conversation.ConversationRepository
import xivo.fullstats.testutils.DBTest

class BridgeEnterSpec extends DBTest(List("xc_call_channel")) {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    implicit val conversationRepository = new ConversationRepository

    val endTimeCaller = format.parseDateTime("2019-01-01 12:05:00")
    val uniqueId = "123456.789"
    val linkedId = "987654.321"
    val channelName = "SIP/larerx9a-00000008"

    val bridgeEnter = new BridgeEnter
    val flowUnderTest = bridgeEnter.flow

    val testSink = TestSink.probe[ConversationLinks]
    def streamUnderTest(cel: BridgeEnterCel) = Source(List(cel)).via(flowUnderTest)

    val cel = EmptyObjectProvider.emptyCel().copy(id = 1, eventTime = endTimeCaller, linkedId = linkedId, chanName = channelName,
      uniqueId = uniqueId, eventType = "BRIDGE_ENTER", context = "default", appName = "AppDial")
  }

  "BridgeEnter Flow" should "add single linked call leg" in new Helper {
    val bridgeEnterCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId)

    val expected = ConversationLinks(linkedId, List(uniqueId))

    streamUnderTest(BridgeEnterCel(bridgeEnterCel)).runWith(testSink).request(2).expectNext(expected).expectComplete()
  }

  it should "add multiple linked call leg" in new Helper {
    val bridgeEnterCel = cel.copy(linkedId = linkedId, uniqueId = "111111.789")

    val conversationLinks = ConversationLinks(linkedId, List(uniqueId))
    conversationRepository.addToMap(linkedId, conversationLinks)

    val expected = ConversationLinks(linkedId, List("111111.789", uniqueId))

    streamUnderTest(BridgeEnterCel(bridgeEnterCel)).runWith(testSink).request(2).expectNext(expected).expectComplete()
  }

  it should "ignore agentcallback context" in new Helper {
    val bridgeEnterCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId, context = "agentcallback")

    streamUnderTest(BridgeEnterCel(bridgeEnterCel)).runWith(testSink).request(1).expectComplete()
  }

  it should "ignore xuc_attended_xfer_wait context" in new Helper {
    val bridgeEnterCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId, context = "xuc_attended_xfer_wait")

    streamUnderTest(BridgeEnterCel(bridgeEnterCel)).runWith(testSink).request(1).expectComplete()
  }

  it should "ignore xuc_attended_xfer_end context" in new Helper {
    val bridgeEnterCel = cel.copy(linkedId = linkedId, uniqueId = uniqueId, context = "xuc_attended_xfer_end")

    streamUnderTest(BridgeEnterCel(bridgeEnterCel)).runWith(testSink).request(1).expectComplete()
  }
}