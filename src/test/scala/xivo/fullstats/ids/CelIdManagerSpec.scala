package xivo.fullstats.ids

import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.Cel
import xivo.fullstats.testutils.DBTest

class CelIdManagerSpec extends DBTest(List("last_cel_id")) {

  "A CelIdManager" should "save the id of the CEL given as parameter" in {
    val cel = EmptyObjectProvider.emptyCel().copy(id=25)
    new CelIdManager().parseEvent(cel)

    Cel.getLastId() shouldEqual 25
  }

}
