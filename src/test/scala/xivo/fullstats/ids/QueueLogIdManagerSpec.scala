package xivo.fullstats.ids

import org.joda.time.DateTime
import xivo.fullstats.model.QueueLog
import xivo.fullstats.testutils.DBTest

class QueueLogIdManagerSpec extends DBTest(List("last_cel_id")) {

   "A QueueLogIdManager" should "save the id of the queue log given as parameter" in {
     val ql = QueueLog(25, new DateTime(), "", "", "", "", None, None, None, None, None)
     new QueueLogIdManager().parseEvent(ql)

     QueueLog.getLastId() shouldEqual 25
   }

 }
