package xivo.fullstats.testutils

import org.scalatest.{FlatSpec, Matchers}

class CsvManager extends FlatSpec with Matchers {

  implicit val connection = DBUtil.getConnection

  "DBUnit" should "create the tables" in {
    pending
    DBUtil.setupDB("database.xml")
  }

  it should "insert from CSV" in {
    pending
    DBUtil.insertFromCsv()
  }

  it should "create cel CSV" in {
    pending
    DBUtil.createCsv("cel")
  }

}
