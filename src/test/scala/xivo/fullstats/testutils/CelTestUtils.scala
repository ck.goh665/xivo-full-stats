package xivo.fullstats.testutils

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm._
import org.joda.time.DateTime
import xivo.fullstats.model.Cel

object CelTestUtils {
    def insertCel(cel: Cel)(implicit c: Connection) {
      SQL("""INSERT INTO cel(id, uniqueid, linkedid, eventtype, eventtime, userdeftype, cid_name, cid_num, cid_ani, cid_rdnis, cid_dnid, exten, context, channame, appname, appdata, amaflags, accountcode, peeraccount, userfield, peer, extra)
        VALUES ({id}, {uniqueid}, {linkedid}, {eventtype}, {eventtime}, '', '', '', '', '', '', '', {context}, '', {appname}, '', 0, '', '', '', {peer}, '')""")
        .on('id -> cel.id, 'uniqueid -> cel.uniqueId, 'linkedid -> cel.linkedId, 'eventtype -> cel.eventType, 'eventtime -> cel.eventTime.toDate,
          'appname -> cel.appName, 'context -> cel.context, 'peer -> cel.peer)
        .executeUpdate
    }
  
    val simple = get[Int]("id") ~
          get[String]("eventtype") ~
          get[Date]("eventtime") ~
          get[String]("userdeftype") ~
          get[String]("cid_name") ~
          get[String]("cid_num") ~
          get[String]("cid_ani") ~
          get[String]("cid_rdnis") ~
          get[String]("cid_dnid") ~
          get[String]("exten") ~
          get[String]("context") ~
          get[String]("channame") ~
          get[String]("appname") ~
          get[String]("appdata") ~
          get[Int]("amaflags") ~
          get[String]("accountcode") ~
          get[String]("peeraccount") ~
          get[String]("uniqueid") ~
          get[String]("linkedid") ~
          get[String]("userfield") ~
          get[String]("peer") ~
          get[String]("extra") ~
          get[Option[Int]]("call_log_id") map {
          case id ~ eventtype ~ eventTime ~ userdeftype ~ cidname ~ cidnum ~ cidani ~ cidrdnis ~ cidDnid ~ exten ~
              context ~ channame ~ appname ~ appdata ~ amaflags ~ accountcode ~ peeraccount ~
              uniqueid ~ linkedid ~ userfield ~ peer ~ extra ~ call_log_id =>
              Cel(id, eventtype, new DateTime(eventTime), userdeftype, cidname, cidnum, cidani, cidrdnis, cidDnid, exten,
                  context, channame, appname, appdata, amaflags, accountcode, peeraccount,
                  uniqueid, linkedid, userfield, peer, extra, call_log_id)
        }
  
    def eventsByAscendingId()(implicit connection: Connection): List[Cel] = SQL(s"select * from cel order by id asc").as(simple *)
}