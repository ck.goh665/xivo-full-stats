package xivo.fullstats.testutils

import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}

class DBTest(tablesToClean: List[String]) extends FlatSpec with Matchers with BeforeAndAfterEach {

  implicit val connection = DBUtil.getConnection
  DBUtil.setupDB("database.xml")
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  val formatMs = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss.SSS")

  override def beforeEach(): Unit = DBUtil.cleanTables(tablesToClean)
}

class DBTestShould(tablesToClean: List[String]) extends BaseTest {

  implicit val connection = DBUtil.getConnection
  DBUtil.setupDB("database.xml")
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  val formatMs = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss.SSS")

  override def beforeEach(): Unit = DBUtil.cleanTables(tablesToClean)
}

