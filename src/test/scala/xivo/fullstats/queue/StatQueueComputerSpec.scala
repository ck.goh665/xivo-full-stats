package xivo.fullstats.queue

import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterEach, Matchers, FlatSpec}
import xivo.fullstats.model.{QueueLog, StatQueuePeriodic}

class StatQueueComputerSpec extends FlatSpec with Matchers with BeforeAndAfterEach {

  var computer: StatQueueComputer = null
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")

  override def beforeEach(): Unit = {
    computer = new StatQueueComputer(
      "2000",
      StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "2000", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
      new Period(0, 15, 0, 0))
  }

  "A StatQueueComputer" should "increment the proper counter" in {
    val ql = QueueLog(1, format.parseDateTime("2014-01-01 08:01:00"), "test", "NONE", "123456.789", "ENTERQUEUE", None, None, None, None, None)
    computer.processQueueLog(ql)
    computer.lastPeriod.total shouldEqual 1

    computer.processQueueLog(ql.copy(event="CLOSED"))
    computer.lastPeriod.closed shouldEqual 1

    computer.processQueueLog(ql.copy(event="JOINEMPTY"))
    computer.lastPeriod.joinEmpty shouldEqual 1

    computer.processQueueLog(ql.copy(event="FULL"))
    computer.lastPeriod.full shouldEqual 1

    computer.processQueueLog(ql.copy(event="DIVERT_CA_RATIO"))
    computer.lastPeriod.divertCaRatio shouldEqual 1

    computer.processQueueLog(ql.copy(event="DIVERT_HOLDTIME"))
    computer.lastPeriod.divertWaitTime shouldEqual 1

    computer.processQueueLog(ql.copy(event="ABANDON"))
    computer.lastPeriod.abandoned shouldEqual 1

    computer.processQueueLog(ql.copy(event="CONNECT"))
    computer.lastPeriod.answered shouldEqual 1

    computer.processQueueLog(ql.copy(event="EXITWITHTIMEOUT"))
    computer.lastPeriod.timeout shouldEqual 1

    computer.processQueueLog(ql.copy(event="LEAVEEMPTY"))
    computer.lastPeriod.leaveEmpty shouldEqual 1

    computer.processQueueLog(ql.copy(event="EXITWITHKEY"))
    computer.lastPeriod.exitWithKey shouldEqual 1
  }

  it should "create a new StatQueuePeriodic and return the old one on new interval notification" in {
    val date = format.parseDateTime("2014-01-01 08:16:00")
    computer.lastPeriod = StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "2000", 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0)

    computer.notifyNewInterval(date) shouldEqual Some(StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00"),
      "2000", 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0))
    computer.lastPeriod shouldEqual StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:15:00"), "2000", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
  }

  it should "return None if the last period is empty" in {
    val date = format.parseDateTime("2014-01-01 08:15:00")
    computer.lastPeriod = StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00"), "2000", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    computer.notifyNewInterval(date) shouldEqual None
    computer.lastPeriod shouldEqual StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:15:00"), "2000", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
  }
}
