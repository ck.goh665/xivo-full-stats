package xivo.fullstats.callparsers.cel

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import xivo.fullstats.model.{CallDirection, HoldPeriod, Transfers}

class TestAnsweredState extends TestState {

  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")

  override def beforeEach() {
    super.beforeEach()
    cel.chanName = "sip/abcd"
    cel.uniqueId = "123456.789"
    cel.linkedId = "123456.789"
  }
  "The Answered state" should "set the answerTime and the status to 'answer'" in {
    cel.eventTime = format.parseDateTime("2014-05-23 14:05:23")

    new Answered(callData, cel)

    callData.status shouldEqual Some("answer")
    callData.answerTime shouldEqual Some(format.parseDateTime("2014-05-23 14:05:23"))
  }

  it should "set the 'transfered' flag and the 'transfer_direction' to 'internal' on BLINDTRANSFER" in {
    cel.eventType = "BLINDTRANSFER"
    val answered = new Answered(callData, cel)

    answered.processCel(cel) should be theSameInstanceAs answered

    callData.transfered shouldBe true
    callData.transferDirection should equal(Some(CallDirection.Internal))
  }

  it should "set the 'transfered' flag and the 'transfer_direction' to 'internal' on ATTENDEDTRANSFER" in {
    cel.eventType = "ATTENDEDTRANSFER"
    val answered = new Answered(callData, cel)

    answered.processCel(cel) should be theSameInstanceAs answered

    callData.transfered shouldBe true
    callData.transferDirection should equal(Some(CallDirection.Internal))
  }

  it should "set the 'transfered' flag and the 'transfer_direction' to 'internal' on XIVO_ATTENDEDTRANSFER_END" in {
    cel.eventType = "XIVO_ATTENDEDTRANSFER_END"
    val answered = new Answered(callData, cel)

    answered.processCel(cel) should be theSameInstanceAs answered

    callData.transfered shouldBe true
    callData.transferDirection should equal(Some(CallDirection.Internal))
  }

  it should "set the 'transfered' flag  and the 'transfer_direction' to 'internal' when Dial starts on the original channel" in {
    cel.eventType = "APP_START"
    cel.appName = "Dial"
    callData.uniqueId = "123456.789"
    cel.uniqueId = callData.uniqueId

    val answered = new Answered(callData, cel)

    answered.processCel(cel) should be theSameInstanceAs answered
    callData.transfered shouldBe true
    callData.transferDirection should equal(Some(CallDirection.Internal))
  }

  it should "parse 'transfer channel' from extra on ATTENDEDTRANSFER" in {
    cel.eventType = "ATTENDEDTRANSFER"
    cel.extra = "{\"bridge1_id\":\"51eb0750-71a3-497c-8be8-8517d580a7fe\",\"channel2_uniqueid\":\"1442841277.34\",\"channel2_name\":\"SIP/gb2hv32m-0000003b\",\"bridge2_id\":\"5da5667b-1fa2-48f3-b41b-526bc37bdf0b\",\"transfer_target_channel_uniqueid\":\"1503911760.136\",\"transferee_channel_name\":\"SIP/ga3te1ei-00000039\",\"transferee_channel_uniqueid\":\"1503911749.129\",\"transfer_target_channel_name\":\"SIP/187w60v8-0000003c\"}"
    val answered = new Answered(callData, cel)

    answered.processCel(cel) should be theSameInstanceAs answered

    callData.transfers should equal(List(Transfers("123456.789","1442841277.34")))
  }

  it should "parse 'transfer channel' from extra on XIVO_ATTENDEDTRANSFER_END" in {
    cel.eventType = "XIVO_ATTENDEDTRANSFER_END"
    cel.extra = "{\"extra\":\"1507631613.36\"}"
    val answered = new Answered(callData, cel)

    answered.processCel(cel) should be theSameInstanceAs answered

    callData.transfers should equal(List(Transfers("123456.789","1507631613.36")))
  }

  it should "close the ongoing hold period when a transfer happens" in {
    cel.eventType = "BLINDTRANSFER"
    cel.eventTime = format.parseDateTime("2014-01-01 08:18:00")
    callData.addHoldPeriod(HoldPeriod(format.parseDateTime("2014-01-01 08:15:45"), Some(format.parseDateTime("2014-01-01 08:16:00")), cel.linkedId))
    callData.addHoldPeriod(HoldPeriod(format.parseDateTime("2014-01-01 08:17:45"), None, cel.linkedId))
    val answered = new Answered(callData, cel)

    answered.processCel(cel)

    callData.holdPeriods shouldEqual List(HoldPeriod(format.parseDateTime("2014-01-01 08:17:45"), Some(cel.eventTime), cel.linkedId),
      HoldPeriod(format.parseDateTime("2014-01-01 08:15:45"), Some(format.parseDateTime("2014-01-01 08:16:00")), cel.linkedId))
  }

  it should "return HangedUp on LINKEDID_END" in {
    cel.eventType = "LINKEDID_END"
    val answered = new Answered(callData, cel)

    answered.processCel(cel).isInstanceOf[HangedUp] shouldBe true
  }

  it should "return HangedUp on HANGUP when in originate" in {
    cel.eventType = "HANGUP"
    callData.statAppName = Some("Originate")
    val answered = new Answered(callData, cel)

    answered.processCel(cel).isInstanceOf[HangedUp] shouldBe true
  }

  it should "stay Answered on HANGUP when not in originate" in {
    cel.eventType = "HANGUP"
    callData.statAppName = None
    val answered = new Answered(callData, cel)

    answered.processCel(cel).isInstanceOf[Answered] shouldBe true
  }

  it should "set the transfer direction to 'outgoing' on XIVO_OUTCALL if the call is flagged with 'transfered'" in {
    callData.transfered = true
    cel.eventType = "XIVO_OUTCALL"
    val answered = new Answered(callData, cel)

    answered.processCel(cel) should be theSameInstanceAs answered
    callData.transferDirection should equal(Some(CallDirection.Outgoing))
  }

  it should "add a HoldPeriod when receiving a HOLD event" in {
    cel.eventType = "HOLD"
    cel.eventTime = format.parseDateTime("2014-01-01 08:15:15")
    val answered = new Answered(callData, cel)

    answered.processCel(cel) should be theSameInstanceAs answered
    callData.holdPeriods shouldEqual List(HoldPeriod(new DateTime(cel.eventTime), None, cel.linkedId))
  }

  it should "close the last HoldPeriod when receiving a UNHOLD event" in {
    cel.eventType = "UNHOLD"
    cel.eventTime = format.parseDateTime("2014-01-01 08:18:00")
    callData.addHoldPeriod(HoldPeriod(format.parseDateTime("2014-01-01 08:15:45"), Some(format.parseDateTime("2014-01-01 08:16:00")), cel.linkedId))
    callData.addHoldPeriod(HoldPeriod(format.parseDateTime("2014-01-01 08:17:45"), None, cel.linkedId))
    val answered = new Answered(callData, cel)

    val res = answered.processCel(cel)

    res should be theSameInstanceAs answered
    callData.holdPeriods shouldEqual List(HoldPeriod(format.parseDateTime("2014-01-01 08:17:45"), Some(cel.eventTime), cel.linkedId),
      HoldPeriod(format.parseDateTime("2014-01-01 08:15:45"), Some(format.parseDateTime("2014-01-01 08:16:00")), cel.linkedId))
  }
}
