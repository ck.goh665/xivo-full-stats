package xivo.fullstats.callparsers.cel

import org.joda.time.DateTime
import org.scalatest.mock.EasyMockSugar
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.{CallElement, CallDirection, CallData}
import xivo.fullstats.testutils.DBTest

class TestChannelManager extends DBTest(List("call_element")) with EasyMockSugar {

  val cd = CallData.insert(CallData(None, "12345.789", None, CallDirection.Incoming, new DateTime(), None, None, None, None, false, None, None))
  var channelMgr: ChannelManager = null

  override def beforeEach(): Unit = {
    super.beforeEach()
    channelMgr = new ChannelManager(cd.id.get)
  }

  "A ChannelManager" should "create channel machines when receiving an APP_START with Dial" in {
    val sip = "SIP/hgthk"
    val sccp = "SCCP/2000"
    val local = "Local/1578548@to-extern"
    val cel = EmptyObjectProvider.emptyCel().copy(eventType = "APP_START", appName = "Dial", appData = s"$sip&$sccp&$local,,")

    channelMgr.processEvent(cel)

    channelMgr.channels.map(_.callElement.copy(id=None)) shouldEqual List(
      CallElement(None, cd.id.get, cel.eventTime, None, None, sip, None),
      CallElement(None, cd.id.get, cel.eventTime, None, None, sccp, None),
      CallElement(None, cd.id.get, cel.eventTime, None, None, local, None))
  }

  it should "not fail with a single interface" in {
    channelMgr.extractInterfaces("SIP/jkljio,,") shouldEqual List("SIP/jkljio")
  }

  it should "not fail with empty arguments" in {
    channelMgr.extractInterfaces("") shouldEqual List()
    channelMgr.extractInterfaces(",,") shouldEqual List()
  }

  it should "create channel machines when receiving an BRIDGE_UPDATE with Dial" in {
    val sip = "SIP/hgthk"
    val sccp = "SCCP/2000"
    val local = "Local/1578548@to-extern"
    val cel = EmptyObjectProvider.emptyCel().copy(eventType = "BRIDGE_UPDATE", appName = "Dial", appData = s"$sip&$sccp&$local,,")

    channelMgr.processEvent(cel)

    channelMgr.channels.map(_.callElement.copy(id=None)) shouldEqual List(
      CallElement(None, cd.id.get, cel.eventTime, Some(cel.eventTime), None, sip, None),
      CallElement(None, cd.id.get, cel.eventTime, Some(cel.eventTime), None, sccp, None),
      CallElement(None, cd.id.get, cel.eventTime, Some(cel.eventTime), None, local, None))
  }

  it should "create single channel machine when receiving an BRIDGE_ENTER with group context" in {
    val sip = "SIP/larerx9a-00000036"
    val cel = EmptyObjectProvider.emptyCel().copy(eventType = "BRIDGE_ENTER", appName = "Queue", context = "group", peer = sip)
    val celWithoutPeer = EmptyObjectProvider.emptyCel().copy(eventType = "BRIDGE_ENTER", appName = "Queue", context = "group")
    val celWithLocalPeer = EmptyObjectProvider.emptyCel().copy(eventType = "BRIDGE_ENTER", appName = "Queue", context = "group", peer = "SIP/Local")

    channelMgr.processEvent(cel)
    channelMgr.processEvent(celWithoutPeer)
    channelMgr.processEvent(celWithLocalPeer)

    channelMgr.channels.map(_.callElement.copy(id=None)) shouldEqual List(
      CallElement(None, cd.id.get, cel.eventTime, Some(cel.eventTime), None, "SIP/larerx9a", None),
    CallElement(None, cd.id.get, celWithLocalPeer.eventTime, Some(celWithLocalPeer.eventTime), None, "SIP/Local", None))
  }

  it should "forward other events to the appropriate ChannelMachine" in {
    val cel = EmptyObjectProvider.emptyCel()
    val machine1 = mock[ChannelMachine]
    val machine2 = mock[ChannelMachine]
    channelMgr.channels.append(machine1, machine2)
    machine1.isEventForMe(cel).andReturn(false)
    machine2.isEventForMe(cel).andReturn(true)
    machine2.processEvent(cel)
    machine2.isChannelHangedUp.andReturn(false)

    whenExecuting(machine1, machine2) {
      channelMgr.processEvent(cel)
      channelMgr.channels shouldEqual List(machine1, machine2)
    }
  }

  it should "remove the machine from the channels list when the channel is hanged up" in {
    val cel = EmptyObjectProvider.emptyCel()
    val machine1 = mock[ChannelMachine]
    val machine2 = mock[ChannelMachine]
    channelMgr.channels.append(machine1, machine2)
    machine1.isEventForMe(cel).andReturn(false)
    machine2.isEventForMe(cel).andReturn(true)
    machine2.processEvent(cel)
    machine2.isChannelHangedUp.andReturn(true)

    whenExecuting(machine1, machine2) {
      channelMgr.processEvent(cel)
      channelMgr.channels shouldEqual List(machine1)
    }
  }

  it should "forward the LINKEDID_END event to all the machines" in {
    val cel = EmptyObjectProvider.emptyCel().copy(eventType = "LINKEDID_END")
    val machine1 = mock[ChannelMachine]
    val machine2 = mock[ChannelMachine]
    channelMgr.channels.append(machine1, machine2)
    machine1.processEvent(cel)
    machine2.processEvent(cel)

    whenExecuting(machine1, machine2) {
      channelMgr.processEvent(cel)
    }
  }

  it should "transfer forceCloture to the ChannelMachines" in {
    val machine1 = mock[ChannelMachine]
    val machine2 = mock[ChannelMachine]
    channelMgr.channels.append(machine1, machine2)
    machine1.forceCloture()
    machine2.forceCloture()

    whenExecuting(machine1, machine2) {
      channelMgr.forceCloture()
    }
  }

}
