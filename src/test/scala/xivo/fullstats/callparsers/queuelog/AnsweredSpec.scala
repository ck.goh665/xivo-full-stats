package xivo.fullstats.callparsers.queuelog

import java.sql.Connection

import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.fullstats.model.{CallExitType, CallOnQueue, QueueLog}

class AnsweredSpec extends FlatSpec with Matchers with BeforeAndAfterEach {

  var state: Answered = null
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  val callid = "123456.789"
  val call = CallOnQueue(None, Some(callid), format.parseDateTime("2013-01-01 08:00:00"), 5, Some(format.parseDateTime("2013-01-01 08:00:30")),
    None, Some(CallExitType.Answered), "queue01", Some("Agent/2000"))
  implicit val c: Connection = null

  override def beforeEach() {
    state = new Answered(call)
  }

  "The Answered state" should "set the talkTime and return Finished on COMPLETEAGENT" in {
    val ql = QueueLog(1, format.parseDateTime("2013-01-01 08:04:00"), callid, "queue01", "Agent/2000", "COMPLETEAGENT",
      Some("30"), Some("126"), Some("2"), None, None)

    val res = state.processEvent(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getResult shouldEqual
      CallOnQueue(None, Some(callid), format.parseDateTime("2013-01-01 08:00:00"), 5, Some(format.parseDateTime("2013-01-01 08:00:30")),
        Some(format.parseDateTime("2013-01-01 08:04:00")), Some(CallExitType.Answered), "queue01", Some("Agent/2000"))
  }

  it should "set the talkTime and return Finished on COMPLETECALLER" in {
    val ql = QueueLog(1, format.parseDateTime("2013-01-01 08:04:00"), callid, "queue01", "Agent/2000", "COMPLETECALLER",
      Some("30"), Some("126"), Some("2"), None, None)

    val res = state.processEvent(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getResult shouldEqual
      CallOnQueue(None, Some(callid), format.parseDateTime("2013-01-01 08:00:00"), 5, Some(format.parseDateTime("2013-01-01 08:00:30")),
        Some(format.parseDateTime("2013-01-01 08:04:00")), Some(CallExitType.Answered), "queue01", Some("Agent/2000"))
  }

  it should "set the talkTime and return Finished on BLINDTRANSFER" in {
    val ql = QueueLog(1, format.parseDateTime("2013-01-01 08:04:00"), callid, "queue01", "Agent/2000", "BLINDTRANSFER",
      Some("1506"), Some("default"), Some("30"), Some("126"), Some("2"))

    val res = state.processEvent(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getResult shouldEqual
      CallOnQueue(None, Some(callid), format.parseDateTime("2013-01-01 08:00:00"), 5, Some(format.parseDateTime("2013-01-01 08:00:30")),
        Some(format.parseDateTime("2013-01-01 08:04:00")), Some(CallExitType.Answered), "queue01", Some("Agent/2000"))
  }

  it should "set the talkTime and return Finished on ATTENDEDTRANSFER" in {
    val ql = QueueLog(1, format.parseDateTime("2013-01-01 08:04:00"), callid, "queue01", "Agent/2000", "ATTENDEDTRANSFER",
      Some("1506"), Some("default"), Some("30"), Some("126"), Some("2"))

    val res = state.processEvent(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getResult shouldEqual
      CallOnQueue(None, Some(callid), format.parseDateTime("2013-01-01 08:00:00"), 5, Some(format.parseDateTime("2013-01-01 08:00:30")),
        Some(format.parseDateTime("2013-01-01 08:04:00")), Some(CallExitType.Answered), "queue01", Some("Agent/2000"))
  }

  it should "set the talkTime and return Finished on TRANSFER" in {
    val ql = QueueLog(1, format.parseDateTime("2013-01-01 08:04:00"), callid, "queue01", "Agent/2000", "TRANSFER",
      Some("1506"), Some("default"), Some("30"), Some("126"), Some("2"))

    val res = state.processEvent(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getResult shouldEqual
      CallOnQueue(None, Some(callid), format.parseDateTime("2013-01-01 08:00:00"), 5, Some(format.parseDateTime("2013-01-01 08:00:30")),
        Some(format.parseDateTime("2013-01-01 08:04:00")), Some(CallExitType.Answered), "queue01", Some("Agent/2000"))
  }
}
