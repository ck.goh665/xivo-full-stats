package xivo.fullstats.callparsers.queuelog

import org.joda.time.DateTime
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model.{CallOnQueue, QueueLog}
import xivo.fullstats.testutils.{CelTestUtils, DBTest}

class FinishedSpec extends DBTest(List("cel")) {

  val uniqueId = "123456.789"
  val linkedId = "123456.788"
  val queueLog = QueueLog(0, new DateTime(), uniqueId, "test", "NONE", "COMPLETECALLER", None, None, None, None, None)

  override def beforeEach(): Unit = {
    super.beforeEach()
    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(uniqueId=uniqueId, linkedId=linkedId))
  }

  "The Finished state" should "search the callid in the CEL if not found before" in {
    val call = CallOnQueue(None, Some(uniqueId), new DateTime(), 0, None, None, None, "test", None)
    call.setLinkedIdFound(false
    )
    val state = new Finished(call, queueLog)

    state.getResult.callid shouldEqual Some(linkedId)
  }

  it should "use the callid of the queue log if it cannot be found in the CEL" in {
    val call = CallOnQueue(None, None, new DateTime(), 0, None, None, None, "test", None)
    call.setLinkedIdFound(false)
    val state = new Finished(call, queueLog.copy(callid = uniqueId + "3"))

    state.getResult.callid shouldEqual Some(uniqueId + "3")
  }
}
