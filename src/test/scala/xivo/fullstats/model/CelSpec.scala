package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm._
import org.joda.time.DateTime
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.testutils.DBTest
import xivo.fullstats.testutils.CelTestUtils

class CelSpec extends DBTest(List("cel")) {

  "The Cel singleton" should "return the linkedid associated to a uniqueid" in {
    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(uniqueId="123456.789",linkedId="123456.777"))

    Cel.linkedIdFromUniqueId("123456.789") shouldEqual Some("123456.777")
    Cel.linkedIdFromUniqueId("12345.999") shouldEqual None
  }

  it should "override lastIdTable with last_cel_id" in {
    Cel.lastIdTable shouldEqual "last_cel_id"
  }

  it should "return the maximum cel id" in {
    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(id=8))
    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(id=12))

    Cel.maximumId shouldEqual Some(12)
  }

  it should "return None as maximum if there is no cel" in {
    Cel.maximumId shouldEqual None
  }

  it should "return the minimum cel id" in {
    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(id=8))
    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(id=12))

    Cel.minimumId shouldEqual Some(8)
  }

  it should "return None as minimum if there is no cel" in {
    Cel.minimumId shouldEqual None
  }

  it should "return the eventtime by id" in {
    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(id=8, eventTime = format.parseDateTime("2015-01-01 08:00:00")))
    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(id=12, eventTime = format.parseDateTime("2015-01-01 09:00:00")))

    Cel.eventTimeById(8) shouldEqual Some(format.parseDateTime("2015-01-01 08:00:00"))
  }

  it should "return the uniqueid from linkedid and parameters" in {
    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(
      id=8, eventTime = format.parseDateTime("2015-01-01 08:00:00"), eventType = "APP_START", appName = "Queue",
      context = "queue", uniqueId = "123456.789", linkedId = "987654.321"))
    Cel.getUniqueIdFromLinkedId("987654.321", "APP_START", "Queue", "queue") shouldEqual Some("123456.789")
  }

  it should "return the linkedid from uniqueid and parameters" in {
    CelTestUtils.insertCel(EmptyObjectProvider.emptyCel().copy(
      id=8, eventTime = format.parseDateTime("2015-01-01 08:00:00"), eventType = "APP_START", appName = "Queue",
      context = "queue", uniqueId = "123456.789", linkedId = "987654.321"))
    Cel.getLinkedIdFromUniqueId("123456.789", "APP_START", "Queue", "queue") shouldEqual Some("987654.321")
  }
}
