package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm.{SQL, ~}
import org.joda.time.DateTime
import xivo.fullstats.testutils.DBTest

class StatAgentPeriodicSpec extends DBTest(List("stat_agent_periodic")) {

  "The StatAgentPeriodic singleton" should "insert or update several StatAgentPeriodic" in {
    val genId = insertStatAgentPeriodic(StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00"), "2000", 1000, 0, 0))

    val stat1 = StatAgentPeriodic(genId.map(_.toInt), format.parseDateTime("2013-01-01 08:00:00"), "2000", 3600, 600, 100)
    val stat2 = StatAgentPeriodic(None, format.parseDateTime("2013-01-01 09:00:00"), "2000", 3600, 600, 100)
    val stat3 = StatAgentPeriodic(None, format.parseDateTime("2013-01-01 10:00:00"), "2000", 3600, 600, 100)
    StatAgentPeriodicManagerImpl.insertAll(List(stat1, stat2, stat3))

    StatAgentPeriodicTestUtils.allStatAgentPeriodic().map(_.copy(id=None)) shouldEqual List(stat1.copy(id=None), stat2, stat3)
  }

  it should "retrieve the last StatAgentPeriodic for the given agent" in {
    insertStatAgentPeriodic(StatAgentPeriodic(None, format.parseDateTime("2013-01-01 07:30:00"), "2000", 1000, 0, 0))
    insertStatAgentPeriodic(StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00"), "2000", 1000, 0, 0))

    StatAgentPeriodicManagerImpl.lastForAgent("2000").map(_.copy(id=None)) shouldEqual
      Some(StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00"), "2000", 1000, 0, 0))

    StatAgentPeriodicManagerImpl.lastForAgent("1000") shouldEqual None
  }

  def insertStatAgentPeriodic(stat: StatAgentPeriodic): Option[Long] = {
    SQL("""INSERT INTO stat_agent_periodic(time, agent, login_time, pause_time, wrapup_time) VALUES
      ({time}, {agent}, {loginTime} * INTERVAL '1 second', {pauseTime} * INTERVAL '1 second', {wrapupTime} * INTERVAL '1 second')""")
      .on('time -> stat.time.toDate, 'agent -> stat.agent, 'loginTime -> stat.loginTime, 'pauseTime -> stat.pauseTime, 'wrapupTime -> stat.wrapupTime)
    .executeInsert()
  }

}

object StatAgentPeriodicTestUtils {

  val simple = get[Date]("time") ~
    get[String]("agent") ~
    get[Double]("login_time") ~
    get[Double]("pause_time") ~
    get[Double]("wrapup_time")  map {
    case time ~ agent~ login~ pause ~ wrapup =>  StatAgentPeriodic(None, new DateTime(time), agent, login.toInt, pause.toInt, wrapup.toInt)
  }

  def allStatAgentPeriodic()(implicit conn: Connection): List[StatAgentPeriodic] =
    SQL("""SELECT time, agent, extract(epoch from login_time) AS login_time,
                             extract(epoch from pause_time) AS pause_time,
                             extract(epoch from wrapup_time) AS wrapup_time FROM stat_agent_periodic""")
      .as(simple *)
}