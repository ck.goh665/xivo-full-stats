drop table if exists agentfeatures;

CREATE TABLE agentfeatures
(
  id serial NOT NULL,
  numgroup integer NOT NULL,
  firstname character varying(128) NOT NULL DEFAULT ''::character varying,
  lastname character varying(128) NOT NULL DEFAULT ''::character varying,
  "number" character varying(40) NOT NULL,
  CONSTRAINT agentfeatures_pkey PRIMARY KEY (id),
  CONSTRAINT agentfeatures_number_key UNIQUE (number)
)