/*********************************/
/*   Enumerations                */
/*********************************/


DROP TYPE IF EXISTS call_termination_type CASCADE;
CREATE TYPE call_termination_type AS ENUM(
  'hangup',
  'remote_hangup'
);

DROP TYPE IF EXISTS call_type CASCADE;
CREATE TYPE call_type AS ENUM(
  'administrative',
  'acd',
  'consultation',
  'queued'
);

DROP TYPE IF EXISTS call_scope_type CASCADE;
CREATE TYPE call_scope_type AS ENUM(
  'external',
  'internal'
);

DROP TYPE IF EXISTS dial_termination_type CASCADE;
CREATE TYPE dial_termination_type AS ENUM(
  'abandoned',
  'denied',
  'established'
);

DROP TYPE IF EXISTS queue_termination_type CASCADE;
CREATE TYPE queue_termination_type AS ENUM(
  'full',
  'closed',
  'joinempty',
  'leaveempty',
  'divert_ca_ratio',
  'divert_waittime',
  'answered',
  'abandoned',
  'timeout',
  'exit_with_key'
);


/*********************************/
/*   Tables with Indices         */
/*********************************/

DROP TABLE IF EXISTS xc_call_channel CASCADE;
CREATE TABLE xc_call_channel (
    id SERIAL PRIMARY KEY,
    start_time TIMESTAMP WITHOUT TIME ZONE,
    end_time TIMESTAMP WITHOUT TIME ZONE,
    direction call_direction_type NOT NULL,
    answered BOOLEAN NOT NULL,
    call_type call_type NOT NULL,
    scope call_scope_type NOT NULL,
    termination call_termination_type,
    channel_interface VARCHAR(255), /* Trunk name, Sip peer name */
    asterisk_id VARCHAR(150) NOT NULL, /* matches asterisk uniqueid */
    user_id INTEGER, /* joins userfeatures */    
    agent_id INTEGER, /* joins agentfeatures */
    dial_id INTEGER, /* joins xc_call_dial */
    queue_call_id INTEGER, /* joins xc_queue_call */
    original_call_id INTEGER
);
CREATE INDEX call_channel__idx__start_time ON xc_call_channel(start_time);
CREATE INDEX call_channel__idx__end_time ON xc_call_channel(end_time);
CREATE INDEX call_channel__idx__asterisk_id ON xc_call_channel(asterisk_id);


DROP TABLE IF EXISTS xc_call_dial CASCADE;
CREATE TABLE xc_call_dial (
    id SERIAL PRIMARY KEY,
    start_time TIMESTAMP WITHOUT TIME ZONE,
    end_time TIMESTAMP WITHOUT TIME ZONE,
    caller VARCHAR(80) NOT NULL,
    callee VARCHAR(80) NOT NULL,
    termination dial_termination_type
);
CREATE INDEX call_dial__idx__start_time ON xc_call_dial(start_time);
CREATE INDEX call_dial__idx__end_time ON xc_call_dial(end_time);


DROP TABLE IF EXISTS xc_call_conversation CASCADE;
CREATE TABLE xc_call_conversation (
    id SERIAL PRIMARY KEY,
    caller_call_id INTEGER NOT NULL, /* joins xc_call_channel */
    callee_call_id INTEGER NOT NULL /* joins xc_call_channel */
);

CREATE INDEX call_conversation__idx__caller_call_id ON xc_call_conversation(caller_call_id);
CREATE INDEX call_conversation__idx__callee_call_id ON xc_call_conversation(callee_call_id);


DROP TABLE IF EXISTS xc_call_transfer CASCADE;
CREATE TABLE xc_call_transfer (
    id SERIAL PRIMARY KEY,
    transfer_time TIMESTAMP WITHOUT TIME ZONE,
    caller_transferee_call_id INTEGER NOT NULL, /* joins xc_call_channel */
    caller_destination_call_id INTEGER NOT NULL, /* joins xc_call_channel */
    transferer_callee_call_id INTEGER NOT NULL, /* joins xc_call_channel */
    transferer_caller_call_id INTEGER NOT NULL /* joins xc_call_channel */
);

CREATE INDEX call_transfer__idx__transfer_time ON xc_call_transfer(transfer_time);
CREATE INDEX call_transfer__idx__caller_transferee_call_id ON xc_call_transfer(caller_transferee_call_id);
CREATE INDEX call_transfer__idx__caller_destination_call_id ON xc_call_transfer(caller_destination_call_id);
CREATE INDEX call_transfer__idx__transferer_callee_call_id ON xc_call_transfer(transferer_callee_call_id);
CREATE INDEX call_transfer__idx__transferer_caller_call_id ON xc_call_transfer(transferer_caller_call_id);


DROP TABLE IF EXISTS xc_queue_call CASCADE;
CREATE TABLE xc_queue_call (
    id SERIAL PRIMARY KEY,
    queue_ref VARCHAR(128), /* joins queuefeatures */
    start_time TIMESTAMP WITHOUT TIME ZONE,
    end_time TIMESTAMP WITHOUT TIME ZONE,
    answer_time TIMESTAMP WITHOUT TIME ZONE,
    ring_time INTEGER,
    termination queue_termination_type,
    agent_id INTEGER, /* joins agentfeatures */
    asterisk_id VARCHAR(150) NOT NULL /* matches asterisk uniqueid */
);

CREATE INDEX queue_call__idx__start_time ON xc_queue_call(start_time);
CREATE INDEX queue_call__idx__end_time ON xc_queue_call(end_time);

/*********************************/
/*   Apply rights                */
/*********************************/

GRANT SELECT ON ALL TABLES IN SCHEMA public TO stats;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public TO stats;
