ALTER TYPE call_termination_type ADD VALUE 'system_hangup';
ALTER TYPE queue_termination_type ADD VALUE 'system_hangup';