DROP INDEX call_dial__idx__start_time;
DROP INDEX call_dial__idx__end_time;
DROP TABLE xc_call_dial;
ALTER TABLE xc_call_channel DROP COLUMN dial_id;

ALTER TABLE xc_call_channel RENAME direction TO emitted;
ALTER TABLE xc_call_channel ALTER emitted TYPE boolean USING CASE emitted WHEN 'outgoing' THEN TRUE ELSE FALSE END;

ALTER TABLE xc_call_transfer RENAME caller_transferee_call_id TO initial_call_id;
ALTER TABLE xc_call_transfer RENAME caller_destination_call_id TO destination_call_id;
ALTER TABLE xc_call_transfer RENAME transferer_caller_call_id TO onhold_call_id;
ALTER TABLE xc_call_transfer RENAME transferer_callee_call_id TO talking_call_id;