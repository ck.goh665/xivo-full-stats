ALTER TABLE xc_call_transfer ALTER COLUMN transferer_callee_call_id DROP NOT NULL;
ALTER TABLE xc_queue_call RENAME ring_time TO ring_duration;
ALTER TABLE xc_call_channel DROP COLUMN answered;
ALTER TABLE xc_call_channel
ADD COLUMN answer_time TIMESTAMP WITHOUT TIME ZONE,
ADD COLUMN ring_duration INTEGER;