CREATE OR REPLACE FUNCTION uuid_generator()
RETURNS TRIGGER
LANGUAGE plpgsql
AS $$
BEGIN
    NEW.uuid := uuid_generate_v4();
    RETURN NEW;
END;
$$;
