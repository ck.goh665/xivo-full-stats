package xivo.fullstats

import xivo.fullstats.iterators.{ClosingHolder, CallEventIterator}
import xivo.fullstats.model.CallEvent

class CallEventList(startCelId: Int, startQueueLogId: Int, additionalCelCids: List[String], additionalQueueLogCids: List[String])(implicit factory: ConnectionFactory)
  extends Iterable[CallEvent] {
  val closingHolder = new ClosingHolder

  override def iterator: Iterator[CallEvent] = new CallEventIterator(startCelId, startQueueLogId, additionalCelCids, additionalQueueLogCids, closingHolder)

  def close(): Unit = closingHolder.close()
}