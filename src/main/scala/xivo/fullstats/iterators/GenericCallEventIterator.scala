package xivo.fullstats.iterators

import java.sql.{PreparedStatement, ResultSet, Connection}

import xivo.fullstats.model.CallEvent

abstract class GenericCallEventIterator[E <: CallEvent](val connection: Connection) extends Iterator[E] {

  val rs: ResultSet = {
    val st = createStatement()
    st.executeQuery()
  }

  def createStatement(): PreparedStatement

  override def hasNext: Boolean = {
    val res = rs.next()
    if(!res)
      connection.close()
    res
  }
}