package xivo.fullstats.iterators

import org.slf4j.LoggerFactory
import xivo.fullstats.ConnectionFactory
import xivo.fullstats.model._

class CallEventIterator(var startCelId: Int, var startQueueLogId: Int, additionalCelCids: List[String], additionalQueueLogCids: List[String], closingHolder: ClosingHolder)
                       (implicit factory: ConnectionFactory) extends Iterator[CallEvent] {

  val batchSize = 50000
  val logger = LoggerFactory.getLogger(getClass)
  implicit var connection = factory.getConnection
  var cel: CelInterface = Cel
  var queueLog: QueueLogInterface = QueueLog

  var maxIds = MaxIds(0, 0)
  setMaxIds()

  var celIterator = new CelIterator(startCelId, maxIds.maxCelId, additionalCelCids, connection)
  var queueLogIterator = new QueueLogIterator(startQueueLogId, maxIds.maxQueueLogId, additionalQueueLogCids, connection)

  var nextCel: Option[Cel] = None
  var nextQueueLog: Option[QueueLog] = None

  setNextCel()
  setNextQueueLog()

  def setNextCel(): Unit = {
    nextCel = if(celIterator.hasNext) Some(celIterator.next()) else None
    startCelId = nextCel.map(_.id + 1).getOrElse(startCelId)
  }

  def setNextQueueLog(): Unit = {
    nextQueueLog = if(queueLogIterator.hasNext) Some(queueLogIterator.next()) else None
    startQueueLogId = nextQueueLog.map(_.id + 1).getOrElse(startQueueLogId)
  }

  def setMaxIds(): Unit = {
    cel.maximumId match {
      case Some(maxCelId) => val realStartCelId = Math.max(startCelId, cel.minimumId.getOrElse(0))
        val chosenCelId = Math.min(maxCelId, realStartCelId + batchSize)
        cel.eventTimeById(chosenCelId) match {
          case None => logger.warn(s"No cel found with id $chosenCelId, keeping old MaxIds")
          case Some(refTime) => val chosenQlId = queueLog.idClosestToTime(refTime).getOrElse(0)
            maxIds = MaxIds(maxCelId = chosenCelId, maxQueueLogId = chosenQlId)
        }
      case None =>
    }

  }

  override def hasNext: Boolean = {
    if(closingHolder.isClosed) return false
    if(nextQueueLog.isEmpty && nextCel.isEmpty)
      false
    else
      true
  }

  def searchNewData(): Unit = {
    resetIterators()
    setNextQueueLog()
    setNextCel()
  }

  private def resetIterators(): Unit = {
    connection.close()
    connection = factory.getConnection
    setMaxIds()
    celIterator = new CelIterator(startCelId, maxIds.maxCelId, List(), connection)
    queueLogIterator = new QueueLogIterator(startQueueLogId, maxIds.maxQueueLogId, List(), connection)
  }

  override def next(): CallEvent = {
    var res: Option[CallEvent] = None
    if(nextQueueLog.isDefined) {
      if(nextCel.isDefined && nextQueueLog.get.time.isAfter(nextCel.get.eventTime)) {
        res = nextCel
        setNextCel()
      } else {
        res = nextQueueLog
        setNextQueueLog()
      }
    } else {
      res = nextCel
      setNextCel()
    }
    res.get
  }
}

case class MaxIds(maxCelId: Int, maxQueueLogId: Int)

class ClosingHolder {
  @volatile var _isClosed = false
  def isClosed = _isClosed
  def close(): Unit = _isClosed = true
}
