package xivo.fullstats.iterators

import java.sql.{PreparedStatement, Connection}

import org.joda.time.DateTime
import xivo.fullstats.model.Cel

class CelIterator(startId: Int, maxId: Int, additionalCids: List[String], override val connection: Connection) extends GenericCallEventIterator[Cel](connection) {

  private  def requestAdditionnalCids(cids: List[String]): String = {
    var idVars = ""
    for (i <- 1 to cids.size)
      idVars += s"?,"
    if(idVars.length > 0)
      idVars = idVars.substring(0, idVars.length - 1)
    s"select * from cel where  linkedid in ( $idVars )"
  }

  override def next(): Cel = Cel(
    rs.getInt("id"),
    rs.getString("eventtype"),
    new DateTime(rs.getTimestamp("eventtime")),
    rs.getString("userdeftype"),
    rs.getString("cid_name"),
    rs.getString("cid_num"),
    rs.getString("cid_ani"),
    rs.getString("cid_rdnis"),
    rs.getString("cid_dnid"),
    rs.getString("exten"),
    rs.getString("context"),
    rs.getString("channame"),
    rs.getString("appname"),
    rs.getString("appdata"),
    rs.getInt("amaflags"),
    rs.getString("accountcode"),
    rs.getString("peeraccount"),
    rs.getString("uniqueid"),
    rs.getString("linkedid"),
    rs.getString("userfield"),
    rs.getString("peer"),
    rs.getString("extra"),
    None)

  override def createStatement(): PreparedStatement = {
    var query = s"select * from cel where id >= $startId AND id <= $maxId order by id asc"
    if(additionalCids.size > 0) {
      query = s"(($query) UNION ${requestAdditionnalCids(additionalCids)}) order by id asc"
    }
    val statement = connection.prepareStatement(query)
    for(i <- 1 to additionalCids.size)
      statement.setString(i, additionalCids(i-1))
    statement
  }
}