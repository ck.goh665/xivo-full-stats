package xivo.fullstats.agent

import java.sql.Connection

import org.joda.time.{DateTime, Period}
import org.slf4j.LoggerFactory
import xivo.fullstats.model._
import xivo.fullstats.{EventParser, TimeUtils}

import scala.collection.mutable

class StatAgentManager(interval: Period, statAgentPeriodicManager: StatAgentPeriodicManager = StatAgentPeriodicManagerImpl)(implicit conn: Connection) extends EventParser[QueueLog] {
  val agentPattern = "Agent/(\\d+)".r
  val AgentEvents = List("AGENTCALLBACKLOGIN", "AGENTCALLBACKLOGOFF", "WRAPUPSTART", "PAUSEALL", "UNPAUSEALL")
  val logger = LoggerFactory.getLogger(getClass)

  val agents = mutable.Map[String, StatAgentComputer]()
  var currentInterval: Option[DateTime] = None

  override def saveState(): Unit = {
    QueueLog.getMaxDate() match {
      case Some(lastDate) => agents.values.foreach(_.saveState(lastDate))
      case None =>
    }
  }

  def extractAgentNumber(s: String): String = {
    agentPattern.findAllIn(s).matchData foreach {
      m => return m.group(1)
    }
    s
  }

  override def parseEvent(e: QueueLog): Unit = {
    val roundedTime = TimeUtils.roundToInterval(e.time, interval)
    if(currentInterval.isEmpty)
      currentInterval = Some(roundedTime)
    else if(roundedTime.isAfter(currentInterval.get)) {
      agents.values.foreach(c => {
        val oldPeriods = c.notifyNewInterval(e.time)
        logger.info(s"Saving periods of type stat_agent_periodic : $oldPeriods on date ${e.time}")
        statAgentPeriodicManager.insertAll(oldPeriods)

      })
      currentInterval = Some(roundedTime)
    }
    if(AgentEvents.contains(e.event))
      realParseEvent(e)
  }

  def realParseEvent(ql: QueueLog): Unit = {
    val agentNum = extractAgentNumber(ql.agent)
    if(!agents.contains(agentNum)) {
      processNewAgent(agentNum, ql)
      logger.info(s"Saving new agent periods $ql")
      statAgentPeriodicManager.insertAll(agents(agentNum).notifyNewInterval(ql.time))
    }
    agents(agentNum).processQueueLog(ql)
  }

  def processNewAgent(agentNum: String, ql: QueueLog): Unit = {
    logger.info(s"A new agent $agentNum was found in the queue_log $ql")
    val periodInterval = TimeUtils.roundToInterval(ql.time, interval)

    val lastPeriod = statAgentPeriodicManager.lastForAgent(agentNum) match {
      case Some(period) => period
      case _ => StatAgentPeriodic(None, periodInterval, agentNum, 0, 0, 0)
    }
    val state = AgentState.lastForAgent(agentNum) match {
      case Some(s)  => s
      case _ => ql.event match {
        case "AGENTCALLBACKLOGIN" => AgentState(periodInterval, State.LoggedOff)
        case _ => AgentState(periodInterval, State.LoggedOn)
      }
    }
    // should not happen
    if(state.date.isAfter(lastPeriod.time.plus(interval))) {
      val period = StatAgentPeriodic(None, TimeUtils.roundToInterval(state.date, interval), agentNum, 0, 0, 0)
      agents.put(agentNum, new StatAgentComputer(agentNum, state, period, interval))
    } else {
      agents.put(agentNum,  new StatAgentComputer(agentNum, state, lastPeriod, interval))
    }
  }
}
