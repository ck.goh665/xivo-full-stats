package xivo.fullstats.agent

import java.sql.Connection

import org.joda.time.DateTime
import xivo.fullstats.model.AgentPosition

object AgentPositionProxy {

  var positionBuffer : Option[AgentPositionBuffer] = None

  def register(buffer: AgentPositionBuffer): Unit = positionBuffer = Some(buffer)

  def agentForNumAndTime(number: String, time: DateTime)(implicit c: Connection): Option[String] = searchInBuffer(number, time) match {
    case None => AgentPosition.agentForNumAndTime(number, time)
    case Some(agent) => Some(agent)
  }

  def searchInBuffer(number: String, time: DateTime) = {
    if(positionBuffer.isDefined)
      positionBuffer.get.allPositions().find(p =>
        (p.lineNumber.equals(number) || p.sda.contains(number)) && (!p.startTime.isAfter(time)) && (p.endTime.isEmpty || p.endTime.get.isAfter(time))).map(_.agent)
    else None
  }
}
