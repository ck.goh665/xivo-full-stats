package xivo.fullstats.model

import java.sql.Connection
import java.util.Date

import akka.stream.Materializer
import anorm.SqlParser._
import anorm._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat


sealed trait TypedQueueLog {
val ql: QueueLog
}
case class EnterQueueQl(ql: QueueLog) extends TypedQueueLog
case class ConnectQl(ql: QueueLog) extends TypedQueueLog
case class CompleteAgentQl(ql: QueueLog) extends TypedQueueLog
case class CompleteCallerQl(ql: QueueLog) extends TypedQueueLog
case class CompleteQl(ql: QueueLog) extends TypedQueueLog
case class QueueTransferQl(ql: QueueLog) extends TypedQueueLog

trait QueueLogInterface {
  def getMaxDate()(implicit c: Connection): Option[DateTime]
  def idClosestToTime(time: DateTime)(implicit c: Connection): Option[Int]
}

case class QueueLog(
  override val id: Int,
  override val time: DateTime,
  override val callid: String,
  queueName: String,
  agent: String,
  event: String,
  data1: Option[String],
  data2: Option[String],
  data3: Option[String],
  data4: Option[String],
  data5: Option[String]) extends CallEvent(id, callid, time)

object QueueLog extends LastIdTable with QueueLogInterface {
  override val lastIdTable: String = "last_queue_log_id"
  val formatter = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss.SSSSSS")

  val queueLog = get[Int]("id") ~
    get[String]("time") ~
    get[String]("callid") ~
    get[String]("queueName") ~
    get[String]("agent") ~
    get[String]("event") ~
    get[Option[String]]("data1") ~
    get[Option[String]]("data2") ~
    get[Option[String]]("data3") ~
    get[Option[String]]("data4") ~
    get[Option[String]]("data5") map {
    case id ~ time ~ callid ~ queueName ~ agent ~ event ~ data1 ~ data2 ~ data3 ~ data4 ~ data5  =>
      QueueLog(id, formatter.parseDateTime(time), callid, queueName, agent, event, data1, data2, data3, data4, data5)
  }

  def getMaxDate()(implicit conn: Connection): Option[DateTime] =
    SQL(s"SELECT to_timestamp(max(time), 'YYYY-MM-DD HH24:MI:SS.US') AS res FROM queue_log").as(get[Option[Date]]("res") *).head.map(new DateTime(_))

  def idClosestToTime(time: DateTime)(implicit c: Connection) = SQL("SELECT max(id) AS max FROM " +
    "(SELECT id FROM queue_log WHERE time <= {time} ORDER BY time DESC LIMIT 2) t").on('time -> time.toString(formatter)).as(get[Option[Int]]("max") *).head

  def getPendingQueueLogsByUniqueIds(uniqueIds: List[String], startId: Int, connection: Connection, batchSize: Int)(implicit materializer: Materializer) = {
    val inClause = SqlUtils.createInClauseOrFalseFast("callid", uniqueIds)

    AkkaStream.source(
      SQL(s"""SELECT
                     id, time, callid, queueName, agent, event, data1, data2, data3, data4, data5
              FROM
                     queue_log
              WHERE
                     $inClause AND id < {startId}
              ORDER BY id ASC""")
        .on('startId -> startId).withFetchSize(Some(batchSize)), queueLog)(materializer, connection)
  }

  def getQueueLogs(startId: Long, maxId: Long)(implicit c: Connection): List[QueueLog] = {
    SQL("SELECT id, time, callid, queueName, agent, event, data1, data2, data3, data4, data5 FROM queue_log WHERE id >= {startId} AND id <= {maxId} ORDER BY id ASC")
      .on('startId -> startId, 'maxId -> maxId)
      .as(queueLog.*)
  }
}
