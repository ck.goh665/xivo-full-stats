package xivo.fullstats.model

object AttachedData {
  val PurgeKey = "xivo_recording_expiration"
}

case class AttachedData (key: String, value: String)