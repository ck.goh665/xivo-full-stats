package xivo.fullstats

import xivo.fullstats.model.CallEvent

trait EventParser[E <: CallEvent] {
  def parseEvent(e: E)
  def saveState()
}
