package xivo.fullstats.callparsers.cel

import xivo.fullstats.model.{Cel, CallData}

class StartedAcd(callData: CallData, cel: Cel) extends CelState(callData, false) {

  override def processCel(cel: Cel): CelState = {
    if (cel.eventType.equals("ANSWER") && cel.uniqueId.equals(cel.linkedId)) {
      result.srcNum = Some(cel.cidNum)
      return new Started(result, cel)
    }
    else if (cel.eventType.equals("LINKEDID_END"))
      return new HangedUp(result, cel)
    this
  }
}
