package xivo.fullstats.callparsers.cel

import org.joda.time.DateTime
import xivo.fullstats.model._

class Answered(callData: CallData, cel: Cel) extends CelState(callData, false) {
  result.status = Some("answer")
  result.answerTime = Some(new DateTime(cel.eventTime))
  
  override def processCel(cel: Cel): CelState = {
    if(cel.eventType.equals("LINKEDID_END")
      || (cel.eventType.equals("HANGUP") && callData.statAppName.contains("Originate"))) {
      return new HangedUp(result, cel)
    } else if(cel.eventType.equals("HOLD")) {
      result.addHoldPeriod(HoldPeriod(new DateTime(cel.eventTime), None, cel.linkedId))
    }else if(cel.eventType.equals("UNHOLD")) {
      result.holdPeriods.headOption.foreach(p => p.end = Some(new DateTime(cel.eventTime)))
    } else if(cel.eventType.equals("BLINDTRANSFER") || cel.eventType.equals("ATTENDEDTRANSFER")) {
      processTransfer(cel)
    } else if (cel.eventType.equals("XIVO_ATTENDEDTRANSFER_END")) {
      processXivoTransfer(cel)
    } else if(cel.eventType.equals("APP_START") && cel.appName.equals("Dial") && cel.uniqueId.equals(result.uniqueId)) {
      processTransfer(cel)
    } else if(result.transfered && cel.eventType.equals("XIVO_OUTCALL")) {
      result.transferDirection = Some(CallDirection.Outgoing)
    }
    this
  }

  private def processTransfer(cel: Cel, transferChannel: String = "channel2_uniqueid") {
    result.transfered = true
    result.transferDirection = Some(CallDirection.Internal)
    result.holdPeriods.headOption.foreach(p => if (p.end.isEmpty) p.end = Some(cel.eventTime))
    CallData.getCallExtraData(transferChannel, cel.extra)
      .foreach(transferChannel => result.addTransfers(cel.eventTime, Transfers(cel.linkedId, transferChannel)))
  }

  private def processXivoTransfer(cel: Cel): Unit = {
    val transferChannel = "extra"
    processTransfer(cel, transferChannel)
  }
}
