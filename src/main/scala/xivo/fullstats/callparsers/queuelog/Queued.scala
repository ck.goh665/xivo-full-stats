package xivo.fullstats.callparsers.queuelog

import java.sql.Connection

import xivo.fullstats.model.{CallExitType, CallOnQueue, QueueLog}

class Queued(call: CallOnQueue)(implicit c: Connection)
    extends QueueLogState(call, false) {
  val agentPattern = "Agent/(\\d+)".r

  def extractAgentNumber(s: String): String = {
    agentPattern.findAllIn(s).matchData foreach { m =>
      return m.group(1)
    }
    s
  }

  override def processEvent(ql: QueueLog): QueueLogState = {
    ql.event match {
      case "ABANDON" =>
        result.hangupTime = Some(ql.time)
        result.status = Some(CallExitType.Abandoned)
        new Finished(result, ql)

      case "EXITWITHTIMEOUT" =>
        result.hangupTime = Some(ql.time)
        result.status = Some(CallExitType.Timeout)
        new Finished(result, ql)

      case "CONNECT" =>
        result.answerTime = Some(ql.time)
        result.ringSeconds += ql.data3.get.toInt
        result.status = Some(CallExitType.Answered)
        result.agentNum = Some(extractAgentNumber(ql.agent))
        new Answered(result)

      case "LEAVEEMPTY" =>
        result.hangupTime = Some(ql.time)
        result.status = Some(CallExitType.LeaveEmpty)
        new Finished(result, ql)

      case "RINGNOANSWER" =>
        result.ringSeconds += ql.data1.get.toInt / 1000
        result.agentNum = Some(extractAgentNumber(ql.agent))
        this

      case "EXITEMPTY" => this

      case "EXITWITHKEY" =>
        result.hangupTime = Some(ql.time)
        result.status = Some(CallExitType.ExitWithKey)
        new Finished(result, ql)

      case "COMPLETEAGENT" | "COMPLETECALLER" | "BLINDTRANSFER" |
          "ATTENDEDTRANSFER" =>
        this

      case _ => processUnexpectedQueueLog(ql)
    }

  }
}
