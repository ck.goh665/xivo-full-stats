package xivo.fullstats.callparsers.queuelog

import java.sql.Connection

import xivo.fullstats.model.{Cel, CallOnQueue, QueueLog}

class Finished(call: CallOnQueue, queueLog: QueueLog)(implicit c: Connection) extends QueueLogState(call, true) {
  if(!call.isLinkedIdFound) {
    call.callid = {
      Cel.linkedIdFromUniqueId(queueLog.callid) match {
        case None => logger.warn(s"Could not find a CEL with uniqueid ${queueLog.callid} in the final state, using the queue log's callid")
          Some(queueLog.callid)
        case Some(linkedId) => Some(linkedId)
      }
    }
  }

  override def processEvent(ql: QueueLog): QueueLogState = processUnexpectedQueueLog(ql)
}
