package xivo.fullstats.callparsers

import java.sql.Connection

import org.joda.time.DateTime
import org.slf4j.LoggerFactory
import xivo.fullstats.EventParser
import xivo.fullstats.agent.AgentPositionProxy
import xivo.fullstats.model._
import xivo.fullstats.callparsers.cel.CelMachine
import xivo.fullstats.callparsers.queuelog.QueueLogMachine

import scala.collection.mutable

object CallSummaryGenerator {
  val LongestCallInHour = 4
}

abstract class CallSummaryGenerator[E <: CallEvent, R <: CallSummary](stateMachineFactory: () => StateMachine[E, R]) extends EventParser[E]{
  val logger = LoggerFactory.getLogger(getClass)
  val callMap = mutable.Map[String, StateMachine[E, R]]()
  var lastPurgeDate: Option[DateTime] = None



  def parseEvent(event: E): Unit = {
    callMap.get(event.callid) match {
      case None => logger.info(s"Processing new call on event $event")
        callMap.put(event.callid, stateMachineFactory())
      case _ =>
    }
    val machine = callMap(event.callid)
    logger.debug(s"Processing event $event")
    machine.processEvent(event)

    if(machine.isCallFinished) {
      val res = machine.getResult
      logger.info(s"The call $res finished with event $event")
      saveResult(res)
      callMap.remove(event.callid)
    }
    purgeIfRequired(event)
  }


  def saveResult(res: Iterable[R])

  def saveState(): Unit = {
    val res = callMap.values.flatMap(_.getResult)
    logger.info(s"Saving the ongoing calls : $res")
    saveResult(res)
  }

  def purgeIfRequired(event: E): Unit = {
    if(lastPurgeDate.isEmpty) {
      lastPurgeDate = Some(event.time)
    }
    if(event.time.isAfter(lastPurgeDate.get.plusHours(CallSummaryGenerator.LongestCallInHour))) {
      doPurge(lastPurgeDate.get)
      lastPurgeDate = Some(event.time)
    }
  }

  def doPurge(time: DateTime): Unit = {
    for((callid, machine) <- callMap) {
      machine.getResult match {
        case Some(call) if call.getStartTime.isBefore(time) =>
          logger.error(s"call $callid never terminate, purge and terminate")
          callMap.remove(callid)
          machine.forceCloture()
          saveResult(machine.getResult)
        case _ =>
      }
    }
  }
}

class CallDataGenerator(implicit c: Connection) extends CallSummaryGenerator[Cel, CallData](() => new CelMachine) {
  override def saveResult(res: Iterable[CallData]): Unit = res.foreach({cd =>
    logger.debug(s"Saving $cd")
    CallData.insertOrUpdate(cd)
  })
}

class CallOnQueueGenerator(implicit c: Connection) extends CallSummaryGenerator[QueueLog, CallOnQueue](() => new QueueLogMachine()) {
  var transferedCalls = Map.empty[String, String]

  override def saveResult(res: Iterable[CallOnQueue]): Unit = res.foreach(CallOnQueue.insertOrUpdate)

  val CallEvents = List("ENTERQUEUE", "CONNECT", "COMPLETEAGENT", "COMPLETECALLER", "TRANSFER","BLINDTRANSFER", "ATTENDEDTRANSFER", "ABANDON", "EXITWITHKEY",
    "EXITWITHTIMEOUT", "CLOSED", "DIVERT_CA_RATIO", "DIVERT_HOLDTIME", "FULL", "LEAVEEMPTY", "JOINEMPTY", "RINGNOANSWER")

  override def parseEvent(ql: QueueLog): Unit = {
    if(CallEvents.contains(ql.event)) {
      val mappedQl = transferedCalls
        .get(ql.callid)
        .map(id => ql.copy(callid = id))
      super.parseEvent(mappedQl.getOrElse(ql))
      mappedQl
        .map(_.callid)
        .filterNot(callMap.contains)
        .foreach(_ =>
          transferedCalls = transferedCalls - ql.callid
        )
    } else if(ql.event == "XIVO_QUEUE_TRANSFER") {
      ql.data1.foreach(linkedid => 
        transferedCalls = transferedCalls.updated(linkedid, ql.callid)
      )
    }
  }
}
