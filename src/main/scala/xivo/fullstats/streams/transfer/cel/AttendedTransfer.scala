package xivo.fullstats.streams.transfer.cel

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model.{AttendedTransferCel, TransferLinks}
import xivo.fullstats.streams.transfer.{TransferFlow, TransferRepository}

class AttendedTransfer(implicit connection: Connection, transferRepository: TransferRepository) extends TransferFlow[AttendedTransferCel] {

  val flow: Flow[AttendedTransferCel, List[TransferLinks], NotUsed] = Flow[AttendedTransferCel]
    .map(keepCel)
    .map(log)
    .via(getTransferLinks)
    .map(processAttendedTransfer)

  private def processAttendedTransfer(maybeTransferLinks: Option[List[TransferLinks]]): List[TransferLinks] = {
    maybeTransferLinks match {
      case Some(transferLinks) => createAndUpdate(transferLinks)
      case None => List()
    }
  }

  private def updateTransferLink(t: TransferLinks): TransferLinks = {
    t.copy(transferDecisionId = Some(savedCel.linkedId), onHold = Some(savedCel.uniqueId), transferTime = Some(savedCel.eventTime))
  }

  private def createAndUpdate(transferLinks: List[TransferLinks]): List[TransferLinks] = {
    transferLinks.flatMap {
      case t if t.transferDecisionId.isEmpty => List(updateTransferLink(t))
      case t if t.transferDecisionId.contains(savedCel.linkedId) =>
        val newTransfer = { if (t.stopCollecting) None else Some(updateTransferLink(t)) }
        val oldTransfer = t.copy(stopCollecting = true)
        List(Some(oldTransfer), newTransfer).flatten
      case t => List(t)
    }
  }
}
