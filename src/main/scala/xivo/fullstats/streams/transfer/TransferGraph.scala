package xivo.fullstats.streams.transfer

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Merge, Sink}
import akka.stream.{ActorAttributes, FlowShape, Graph}
import xivo.fullstats.model._
import xivo.fullstats.streams.CallEventsGraph
import xivo.fullstats.streams.transfer.cel.{AttendedTransfer, BlindTransfer, BridgeEnter, BridgeExit, LinkedIdEnd, XivoAttendedTransferEnd, XivoAttendedTransferWait}

class TransferGraph (implicit val connection: Connection, transferRepository: TransferRepository)
  extends CallEventsGraph[FlowShape[CallEvent, TransferCall], NotUsed] {

  val attendedTransferFlow = new AttendedTransfer
  val blindTransferFlow = new BlindTransfer
  val bridgeEnterFlow = new BridgeEnter
  val bridgeExitFlow = new BridgeExit
  val linkedIdEndFlow = new LinkedIdEnd
  val transferLinkToTransferCallFlow = new TransferLinkToTransferCall
  val xivoAtxWaitFlow = new XivoAttendedTransferWait
  val xivoAtxEndFlow = new XivoAttendedTransferEnd

  val graph: Graph[FlowShape[CallEvent, TransferCall], NotUsed] = GraphDSL.create() {
    implicit builder =>

      import GraphDSL.Implicits._

      val cel = builder.add(Flow[CallEvent].collect { case e: Cel => e })

      val converter = builder.add(Flow[Cel].map(convertCel).collect { case Some(e) => e })

      val attendedTransfer = builder.add(Flow[TypedCelEvent]
        .collect { case e: AttendedTransferCel => e }
        .via(attendedTransferFlow.flow))

      val blindTransfer = builder.add(Flow[TypedCelEvent]
        .collect { case e: BlindTransferCel => e }
        .via(blindTransferFlow.flow))

      val bridgeEnter = builder.add(Flow[TypedCelEvent]
        .collect { case e: BridgeEnterCel => e }
        .via(bridgeEnterFlow.flow))

      val bridgeExit = builder.add(Flow[TypedCelEvent]
        .collect{ case e: BridgeExitCel => e }
        .via(bridgeExitFlow.flow))

      val xivoAtxWait = builder.add(Flow[TypedCelEvent]
        .collect{ case e: XivoAttendedTransferWaitCel => e }
        .via(xivoAtxWaitFlow.flow))

      val xivoAtxEnd = builder.add(Flow[TypedCelEvent]
        .collect{ case e: XivoAttendedTransferEndCel => e }
        .via(xivoAtxEndFlow.flow))

      val linkedIdEnd = builder.add(Flow[TypedCelEvent]
        .collect { case e: LinkedIdEndCel => e }
        .via(linkedIdEndFlow.flow))

      val transferLinkToTransferCall = builder.add(transferLinkToTransferCallFlow.flow)

      val finish = builder.add(Flow[TransferCall])

      val updateInRepository = Flow[List[TransferLinks]]
        .collect{ case l if l.nonEmpty => l }
        .map(e => transferRepository.addToMap(e.head.linkedId, e))
        .map{ e => logger.info(s"Saved in repository $e"); e }

      val broadcast = builder.add(Broadcast[TypedCelEvent](7))
      val merge = builder.add(Merge[List[TransferLinks]](6))

      val sink = Sink.ignore

      cel ~> converter ~> broadcast

      broadcast ~> bridgeEnter      ~> merge ~> updateInRepository         ~> sink
      broadcast ~> bridgeExit       ~> merge
      broadcast ~> xivoAtxWait      ~> merge
      broadcast ~> xivoAtxEnd       ~> merge
      broadcast ~> attendedTransfer ~> merge
      broadcast ~> blindTransfer    ~> merge

      broadcast ~> linkedIdEnd               ~> transferLinkToTransferCall ~> finish

      FlowShape(cel.in, finish.out)
  }.withAttributes(ActorAttributes.supervisionStrategy(decider))
}
