package xivo.fullstats.streams.conversation

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Merge, Sink}
import akka.stream.{ActorAttributes, FlowShape, Graph}
import xivo.fullstats.model._
import xivo.fullstats.streams.CallEventsGraph
import xivo.fullstats.streams.conversation.cel.{BridgeEnter, BridgeExit, ChanEnd, LinkedIdEnd}

class ConversationGraph(implicit val connection: Connection, conversationRepository: ConversationRepository)
  extends CallEventsGraph[FlowShape[CallEvent, Conversation], NotUsed] {

  val bridgeEnterFlow = new BridgeEnter
  val bridgeExitFlow = new BridgeExit
  val chanEndFlow = new ChanEnd
  val linkedIdEndFlow = new LinkedIdEnd

  val graph: Graph[FlowShape[CallEvent, Conversation], NotUsed] = GraphDSL.create() {
    implicit builder =>

      import GraphDSL.Implicits._

      val cel = builder.add(Flow[CallEvent].collect { case e: Cel => e })

      val converter = builder.add(Flow[Cel].map(convertCel).collect { case Some(e) => e })

      val bridgeEnter = builder.add(Flow[TypedCelEvent]
        .collect { case e: BridgeEnterCel => e }
        .via(bridgeEnterFlow.flow))

      val bridgeExit = builder.add(Flow[TypedCelEvent]
        .collect{ case e: BridgeExitCel => e }
        .via(bridgeExitFlow.flow))

      val chanEnd = builder.add(Flow[TypedCelEvent]
        .collect { case e: ChanEndCel => e }
        .via(chanEndFlow.flow))

      val linkedIdEnd = builder.add(Flow[TypedCelEvent]
        .collect { case e: LinkedIdEndCel => e }
        .via(linkedIdEndFlow.flow))

      val finish = builder.add(Flow[Conversation])

      val updateInRepository = Flow[ConversationLinks]
        .map(e => conversationRepository.addToMap(e.linkedId, e))
        .map{ e => logger.info(s"Saved in repository $e"); e }

      val broadcast = builder.add(Broadcast[TypedCelEvent](4))
      val merge = builder.add(Merge[ConversationLinks](2))
      val sink = Sink.ignore

      cel ~> converter ~> broadcast

      broadcast ~> bridgeEnter          ~> merge ~> updateInRepository ~> sink
      broadcast ~> bridgeExit           ~> merge
      broadcast ~> linkedIdEnd                                         ~> sink
      broadcast ~> chanEnd                                             ~> finish

      FlowShape(cel.in, finish.out)
  }.withAttributes(ActorAttributes.supervisionStrategy(decider))
}
