package xivo.fullstats.streams

import akka.stream.{Graph, Shape, Supervision}
import org.slf4j.{Logger, LoggerFactory}
import xivo.fullstats.model._

trait CallEventsGraph[S <: Shape, R] {
  val logger: Logger = LoggerFactory.getLogger(getClass)

  val graph: Graph[S, R]

  val decider: Supervision.Decider = {
    case e: Exception =>
      logger.error(s"Exception occurred: $e - ${e.printStackTrace()}")
      Supervision.Resume
  }

  def convertQueueLog(ql: QueueLog): Option[TypedQueueLog] = {
    val completeEvents = List("COMPLETEAGENT", "COMPLETECALLER", "TRANSFER", "BLINDTRANSFER", "ATTENDEDTRANSFER", "ABANDON", "EXITWITHKEY",
      "EXITWITHTIMEOUT", "CLOSED", "DIVERT_CA_RATIO", "DIVERT_HOLDTIME", "FULL", "LEAVEEMPTY", "JOINEMPTY")

    ql.event match {
      case "ENTERQUEUE" => Some(EnterQueueQl(ql))
      case "CONNECT" => Some(ConnectQl(ql))
      case "XIVO_QUEUE_TRANSFER" => Some(QueueTransferQl(ql))
      case e if completeEvents.contains(e) => Some(CompleteQl(ql))
      case _ => None
    }
  }

  def convertCel(cel: Cel): Option[TypedCelEvent] = {
    cel.eventType match {
      case "CHAN_START" => Some(ChanStartCel(cel))
      case "XIVO_INCALL" => Some(XivoIncallCel(cel))
      case "XIVO_OUTCALL" => Some(XivoOutcallCel(cel))
      case "HANGUP" => Some(HangupCel(cel))
      case "LINKEDID_END" => Some(LinkedIdEndCel(cel))
      case "ANSWER" => Some(AnswerCel(cel))
      case "CHAN_END" => Some(ChanEndCel(cel))
      case "APP_START" => Some(AppStartCel(cel))
      case "BRIDGE_ENTER" => Some(BridgeEnterCel(cel))
      case "BRIDGE_EXIT" => Some(BridgeExitCel(cel))
      case "ATTENDEDTRANSFER" => Some(AttendedTransferCel(cel))
      case "BLINDTRANSFER" => Some(BlindTransferCel(cel))
      case "XIVO_ATTENDEDTRANSFER_END" => Some(XivoAttendedTransferEndCel(cel))
      case "XIVO_ATTENDEDTRANSFER_WAIT" => Some(XivoAttendedTransferWaitCel(cel))
      case _ => None
    }
  }
}
