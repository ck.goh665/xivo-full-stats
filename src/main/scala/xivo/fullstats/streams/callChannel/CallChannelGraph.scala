package xivo.fullstats.streams.callChannel

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Merge}
import akka.stream.{ActorAttributes, FlowShape, Graph}
import xivo.fullstats.model._
import xivo.fullstats.streams.CallEventsGraph
import xivo.fullstats.streams.callChannel.cel._

class CallChannelGraph(implicit val connection: Connection, callChannelRepository: CallChannelRepository)
  extends CallEventsGraph[FlowShape[CallEvent, CallChannel], NotUsed] {

  val chanStartFlow = new ChanStart
  val xivoIncallFlow = new XivoIncall
  val xivoOutcallFlow = new XivoOutcall
  val hangupFlow = new Hangup
  val linkedIdEndFlow = new LinkedIdEnd
  val answerFlow = new Answer
  val chanEndFlow = new ChanEnd
  val appStartFlow = new AppStart

  val graph: Graph[FlowShape[CallEvent, CallChannel], NotUsed] = GraphDSL.create() {
    implicit builder =>

      import GraphDSL.Implicits._

      val cel = builder.add(Flow[CallEvent].collect { case e: Cel => e })

      val converter = Flow[Cel].map(convertCel).collect { case Some(e) => e }
      val convertedCelBroadcast = builder.add(Broadcast[TypedCelEvent](8))

      val chanStart = Flow[TypedCelEvent].collect { case e: ChanStartCel => e }.via(chanStartFlow.flow)
      val appStart = Flow[TypedCelEvent].collect{ case e: AppStartCel => e }.via(appStartFlow.flow)
      val answer = Flow[TypedCelEvent].collect{ case e: AnswerCel => e }.via(answerFlow.flow)
      val xivoIncall = Flow[TypedCelEvent].collect { case e: XivoIncallCel => e }.via(xivoIncallFlow.flow )
      val xivoOutcall = Flow[TypedCelEvent].collect{ case e: XivoOutcallCel => e }.via(xivoOutcallFlow.flow)
      val chanEnd = Flow[TypedCelEvent].collect{ case e: ChanEndCel => e }.via(chanEndFlow.flow)
      val hangup = Flow[TypedCelEvent].collect{ case e: HangupCel => e }.via(hangupFlow.flow)
      val linkedIdEnd = Flow[TypedCelEvent].collect{ case e: LinkedIdEndCel => e }.via(linkedIdEndFlow.flow)

      val merge = builder.add(Merge[CallChannel](8))

      val finishAndSave = builder.add(Flow[CallChannel])

      cel ~> converter ~> convertedCelBroadcast ~> chanStart   ~> merge  ~> finishAndSave
                          convertedCelBroadcast ~> xivoIncall  ~> merge
                          convertedCelBroadcast ~> xivoOutcall ~> merge
                          convertedCelBroadcast ~> hangup      ~> merge
                          convertedCelBroadcast ~> answer      ~> merge
                          convertedCelBroadcast ~> appStart    ~> merge
                          convertedCelBroadcast ~> chanEnd     ~> merge
                          convertedCelBroadcast ~> linkedIdEnd ~> merge

      FlowShape(cel.in, finishAndSave.out)
  }.withAttributes(ActorAttributes.supervisionStrategy(decider))
}
