package xivo.fullstats.streams.callChannel.cel

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.{CallChannelFlow, CallChannelRepository}

class XivoIncall(implicit repository: CallChannelRepository) extends CallChannelFlow[XivoIncallCel] {

  val flow: Flow[XivoIncallCel, CallChannel, NotUsed] = Flow[XivoIncallCel]
    .map(keepCel)
    .map(log)
    .via(getCallChannel)
    .map(updateCallChannelScope)
    .map(finishAndSave)

  private def updateCallChannelScope(callChannel: CallChannel) = {
    callChannel.copy(
      emitted = false,
      scope = CallScope.External
    )
  }
}
