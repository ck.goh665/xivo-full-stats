package xivo.fullstats.streams

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.Flow
import org.slf4j.LoggerFactory
import xivo.fullstats.model.CallThreadEvent
import xivo.fullstats.streams.callChannel.CallChannelRepository
import xivo.fullstats.streams.queueCall.QueueCallRepository

class DatabaseActions(implicit val connection: Connection, callChannelRepository: CallChannelRepository,
                      queueCallRepository: QueueCallRepository) {

  val logger = LoggerFactory.getLogger(getClass)

  def updateInRepository: Flow[CallThreadEvent, CallThreadEvent, NotUsed] = Flow[CallThreadEvent]
    .map(e => e.addToRepository(e))
    .map{ e => logger.info(s"Saved in repository $e"); e }

  def updateInDatabase: Flow[CallThreadEvent, CallThreadEvent, NotUsed] = Flow[CallThreadEvent]
    .map(e => e.insertOrUpdate(e))
    .map{ e => logger.info(s"Saved in database $e"); e }
}
