package xivo.fullstats.streams.source

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.{Concat, Source}
import akka.stream.{ActorAttributes, ActorMaterializer, Supervision, ThrottleMode}
import org.joda.time.DateTime
import org.slf4j.{Logger, LoggerFactory}
import xivo.fullstats.iterators.ClosingHolder
import xivo.fullstats.model.{QueueCall, QueueLog}

import scala.concurrent.duration._
import scala.util.{Failure, Success}

class QueueLogSource(startQueueLogId: Int, closingHolder: ClosingHolder, pendingStartTime: Option[DateTime], connectionForPendingQl: Connection)(implicit val c: Connection, materializer: ActorMaterializer) {
  import materializer.executionContext
  final val BATCH_SIZE = 50000

  val logger: Logger = LoggerFactory.getLogger(getClass)

  val decider: Supervision.Decider = {
    case e: Exception =>
      logger.error(s"Exception occurred: $e - ${e.printStackTrace()}")
      Supervision.Resume
  }

  QueueCall.setStalePendingCalls
  private val pendingQueueLogsUniqueIds: List[String] = getPendingQueueLogs(pendingStartTime)
  logger.info(s"Removing pending queue calls for table xc_queue_call : $pendingQueueLogsUniqueIds")
  QueueCall.deleteById(pendingQueueLogsUniqueIds)

  val pendingQueueLogsSource: Source[QueueLog, Unit] = {
    QueueLog.getPendingQueueLogsByUniqueIds(pendingQueueLogsUniqueIds, startQueueLogId, connectionForPendingQl, BATCH_SIZE)
      .mapMaterializedValue(_.onComplete {
        case Success(s) => logger.info(s"Finished retrieving $s pending queue logs")
          connectionForPendingQl.close()
        case Failure(f) => logger.error(s"Error while retrieving the pending queue logs: $f")
          connectionForPendingQl.close()
      })
  }

  val queueLogSource: Source[QueueLog, NotUsed] =
    Source.unfold(startQueueLogId)(fetchQueueLogs)
      .withAttributes(ActorAttributes.supervisionStrategy(decider))
      .throttle(1, 100.millis, 1, ThrottleMode.shaping)
      .flatMapConcat { queueLogs => Source.fromIterator(() => queueLogs.iterator) }

  val source: Source[QueueLog, NotUsed] = Source.combine(pendingQueueLogsSource, queueLogSource)(Concat(_))

  def getPendingQueueLogs: Option[DateTime] => List[String] = {
    case Some(t) => QueueCall.getPendingUniqueIds(t)
    case None => List()
  }

  private def fetchQueueLogs: Int => Option[(Int, List[QueueLog])] = { qlId: Int =>
    if (closingHolder.isClosed) {
      None
    }
    else {
      val res: List[QueueLog] = QueueLog.getQueueLogs(qlId, qlId + BATCH_SIZE)
      val lastId = if (res.isEmpty) qlId else res.last.id + 1
      Some(lastId, res)
    }
  }
}
