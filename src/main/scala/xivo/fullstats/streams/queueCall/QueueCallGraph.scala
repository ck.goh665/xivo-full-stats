package xivo.fullstats.streams.queueCall

import java.sql.Connection

import akka.NotUsed
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Merge}
import akka.stream.{ActorAttributes, FlowShape, Graph}
import xivo.fullstats.model._
import xivo.fullstats.streams.CallEventsGraph
import xivo.fullstats.streams.queueCall.queuelog.{Complete, Connect, EnterQueue, QueueTransfer}

class QueueCallGraph(implicit val connection: Connection, queueCallRepository: QueueCallRepository)
  extends CallEventsGraph[FlowShape[CallEvent, QueueCall], NotUsed] {

  val enterQueueFlow = new EnterQueue
  val connectFlow = new Connect
  val completeFlow = new Complete
  val queueTransferFlow = new QueueTransfer

  val graph: Graph[FlowShape[CallEvent, QueueCall], NotUsed] = GraphDSL.create() {
    implicit builder =>

      import GraphDSL.Implicits._

      val ql = builder.add(Flow[CallEvent].collect { case e: QueueLog => e })

      val converter = Flow[QueueLog].map(convertQueueLog).collect { case Some(e) => e }
      val convertedQlBroadcast = builder.add(Broadcast[TypedQueueLog](4))

      val enterQueue = Flow[TypedQueueLog].collect{ case e: EnterQueueQl => e }.via(enterQueueFlow.flow)
      val connect = Flow[TypedQueueLog].collect{ case e: ConnectQl => e }.via(connectFlow.flow)
      val completeGeneric = Flow[TypedQueueLog].collect{ case e: CompleteQl => e }.via(completeFlow.flow)
      val transferred = Flow[TypedQueueLog].collect{ case e: QueueTransferQl => e }.via(queueTransferFlow.flow)

      val merge = builder.add(Merge[QueueCall](4))

      val finishAndComplete = builder.add(Flow[QueueCall])

      ql ~> converter ~> convertedQlBroadcast ~> enterQueue      ~> merge  ~> finishAndComplete
                         convertedQlBroadcast ~> connect         ~> merge
                         convertedQlBroadcast ~> transferred     ~> merge
                         convertedQlBroadcast ~> completeGeneric ~> merge

      FlowShape(ql.in, finishAndComplete.out)
  }.withAttributes(ActorAttributes.supervisionStrategy(decider))
}
