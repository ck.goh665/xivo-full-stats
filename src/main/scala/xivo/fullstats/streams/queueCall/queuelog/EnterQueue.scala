package xivo.fullstats.streams.queueCall.queuelog

import akka.NotUsed
import akka.stream.scaladsl.Flow
import xivo.fullstats.model.{EnterQueueQl, QueueCall}
import xivo.fullstats.streams.queueCall.{QueueCallFlow, QueueCallRepository}

class EnterQueue(implicit repository: QueueCallRepository) extends QueueCallFlow[EnterQueueQl] {

  val flow: Flow[EnterQueueQl, QueueCall, NotUsed] = Flow[EnterQueueQl]
    .map(keepQl)
    .map(log)
    .map(_ => createQueueCall)
    .map(finishAndSave)

  private def createQueueCall: QueueCall = QueueCall(
    id = None,
    queueRef = savedQl.queueName,
    startTime = savedQl.time,
    endTime = None,
    termination = None,
    answerTime = None,
    ringDuration = None,
    agentId = None,
    uniqueId = savedQl.callid,
    deleted = false,
    transferredCalls = Map.empty
  )
}
