package xivo.fullstats.streams.common

import java.sql.Connection

import akka.NotUsed
import akka.actor.{Actor, Cancellable, Props, Scheduler}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import org.slf4j.LoggerFactory
import xivo.fullstats.EmptyObjectProvider
import xivo.fullstats.model._
import xivo.fullstats.streams.callChannel.CallChannelRepository
import xivo.fullstats.streams.common.PeriodicCleanPendingCalls._
import xivo.fullstats.streams.conversation.ConversationRepository
import xivo.fullstats.streams.conversation.cel.ChanEnd
import xivo.fullstats.streams.queueCall.QueueCallRepository
import xivo.fullstats.streams.transfer.cel.LinkedIdEnd
import xivo.fullstats.streams.transfer.{TransferLinkToTransferCall, TransferRepository}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

object PeriodicCleanPendingCalls {
  type Filter = CallThreadEvent => Boolean
  def props(callChannelRepository: CallChannelRepository, queueCallRepository: QueueCallRepository,
            conversationRepository: ConversationRepository, transferRepository: TransferRepository,
            scheduler: Scheduler)(implicit connection: Connection, materializer: ActorMaterializer) =
    Props(new PeriodicCleanPendingCalls(callChannelRepository, queueCallRepository, conversationRepository, transferRepository, scheduler))

  case object Init
  case object CleanCallChannels
  case object CleanQueueCalls
  case class CleanConversations(closed: List[CallChannel])
  case class CleanTransfers(closed: List[CallChannel])
}

class PeriodicCleanPendingCalls(callChannelRepository: CallChannelRepository, queueCallRepository: QueueCallRepository,
                                conversationRepository: ConversationRepository, transferRepository: TransferRepository,
                                scheduler: Scheduler)(implicit val connection: Connection, materializer: ActorMaterializer) extends Actor {
  val logger = LoggerFactory.getLogger(getClass)

  final val INTERVAL = 12.hours
  final val CLOSING_LIMIT = 4.hours

  def scheduleCallChannels: Cancellable = scheduler.schedule(INTERVAL, INTERVAL, self, CleanCallChannels)(context.dispatcher)
  def scheduleQueueCalls: Cancellable = scheduler.schedule(INTERVAL, INTERVAL, self, CleanQueueCalls)(context.dispatcher)

  override def preStart(): Unit = {
    self ! Init
    super.preStart()
  }

  override def receive: Receive = {
    case Init =>
      scheduleCallChannels
      scheduleQueueCalls
    case CleanCallChannels =>
      futureCloseCallChannels().map{ closed =>
        closed.foreach(c => callChannelRepository.deleteCallFromMap(c.uniqueId))
        self ! CleanConversations(closed)
        self ! CleanTransfers(closed)
      }
    case CleanConversations(closed) => closeConversations(closed)
    case CleanTransfers(closed) => closeTransfers(closed)
    case CleanQueueCalls => closeQueueCalls()
  }

  def futureCloseCallChannels(): Future[List[CallChannel]] = {
    Future {
      callChannelsAfterLimit.map(c => CallChannel.insertOrUpdate(c.copy(endTime = Some(c.startTime), termination = Some(CallTermination.SystemHangup)))).toList
    }.map{ closed =>
      logger.info(s"Closed $CLOSING_LIMIT pending call channels  ${closed.map(_.uniqueId)}")
      closed
    }
  }

  def closeQueueCalls(): Future[Unit] = {
    Future {
      queueCallsAfterLimit.map(q => QueueCall.insertOrUpdate(q.copy(endTime = Some(q.startTime), termination = Some(QueueTerminationType.SYSTEM_HANGUP))))
    }.map { closed =>
      logger.info(s"Closed $CLOSING_LIMIT pending queue calls ${closed.map(_.uniqueId)}")
      closed.foreach(q => queueCallRepository.deleteCallFromMap(q.uniqueId))
    }
  }

  def closeConversations(callChannels: List[CallChannel]): NotUsed = {
    implicit val repo: ConversationRepository = conversationRepository
    val chanEnd = new ChanEnd

    val cel: List[ChanEndCel] = callChannels.map(c => ChanEndCel(EmptyObjectProvider.emptyCel().copy(linkedId = c.uniqueId)))

    Source(cel).via(chanEnd.flow).to(Sink.foreach { c => Conversation.insertOrUpdate(c)}).run()
  }

  def closeTransfers(callChannels: List[CallChannel]): NotUsed = {
    implicit val repo: TransferRepository = transferRepository
    val linkedIdEnd = new LinkedIdEnd
    val transfer = new TransferLinkToTransferCall

    val cel: List[LinkedIdEndCel] = callChannels.map(c => LinkedIdEndCel(EmptyObjectProvider.emptyCel().copy(linkedId = c.uniqueId)))

    Source(cel).via(linkedIdEnd.flow).via(transfer.flow).to(Sink.foreach { t => TransferCall.insertOrUpdate(t)}).run()
  }

  def callChannelsAfterLimit: Iterable[CallChannel] = callChannelRepository.repository.values.filter(isPendingForTooLong)
  def queueCallsAfterLimit: Iterable[QueueCall] = queueCallRepository.repository.values.filter(isPendingForTooLong)

  def isPendingForTooLong: Filter = {
    case c: CallChannel => c.startTime.plus(CLOSING_LIMIT.toMillis).isBeforeNow && c.endTime.isEmpty
    case q: QueueCall => q.startTime.plus(CLOSING_LIMIT.toMillis).isBeforeNow && q.endTime.isEmpty
    case _ => false
  }
}
