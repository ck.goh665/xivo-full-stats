#!/bin/bash

readonly progname="${0##*/}"

# Exclude all 
# - generated tables by xivo_stats (old/new model) or pack_reporting
# - and cel + queue_log
EXCLUDE_TABLES="\
    -T attached_data \
    -T call_data \
    -T call_element \
    -T call_on_queue \
    -T hold_periods \
    -T transfers \
    -T stat_agent_periodic \
    -T stat_agent_queue_specific \
    -T stat_agent_specific \
    -T stat_queue_periodic \
    -T stat_queue_specific \
    -T last_cel_id \
    -T last_queue_log_id \
    -T xc_call_channel \
    -T xc_call_conversation \
    -T xc_call_transfer \
    -T xc_queue_call \
    -T cel \
    -T queue_log \
    "
PG_DUMP_OPTIONS="--data-only --schema=public $EXCLUDE_TABLES"

dump_static() {
    local ouptut=${1}; shift

    pg_dump -h localhost \
        -p 5443 \
        -U stats \
        $PG_DUMP_OPTIONS \
        xivo_stats \
        -f "${ouptut}"
}

dump_queuelog() {
    local ouptut=${1}; shift
    local from=${1}; shift
    local to=${1}; shift

    psql -h localhost \
        -p 5443 \
        -U stats \
        xivo_stats \
        -c "\copy (SELECT * FROM queue_log WHERE time BETWEEN '${from}' AND '${to}') TO '${ouptut}'"
}


dump_cel() {
    local ouptut=${1}; shift
    local from=${1}; shift
    local to=${1}; shift

    psql -h localhost \
        -p 5443 \
        -U stats \
        xivo_stats \
        -c "\copy (SELECT * FROM cel WHERE eventtime BETWEEN '${from}' AND '${to}') TO '${ouptut}'"
}

usage() {
    echo "Usage:"
    echo "${progname} [backupfile] [date from] [date to]"
    echo -e "\t- backupfile: path to backupfile"
    echo -e "\t    backup will then be stored in:"
    echo -e "\t      <backupfile>.sql"
    echo -e "\t      <backupfile>_cel_extract.sql"
    echo -e "\t      <backupfile>_queuelog_extract.sql"
    echo -e "\t- date from: date from which to export CEL and queue_log"
    echo -e "\t    YYYY-MM-DD HH:MM:SS"
    echo -e "\t- date to: date till which to export CEL and queue_log"
    echo -e "\t    YYYY-MM-DD HH:MM:SS"

    exit 1
}

main() {
    [ $# -ne 3 ] && usage

    local backupfile
    local date_from
    local date_to
    backupfile="${1}"
    date_from="${2}"
    date_to="${3}"

    dump_static "${backupfile}.sql"
    dump_cel "${backupfile}_cel_extract.sql" "${date_from}" "${date_to}"
    dump_queuelog "${backupfile}_queuelog_extract.sql" "${date_from}" "${date_to}"
}

main "${@}"